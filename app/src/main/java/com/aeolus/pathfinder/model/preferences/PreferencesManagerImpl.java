package com.aeolus.pathfinder.model.preferences;

import android.content.Context;

public class PreferencesManagerImpl implements PreferencesManager {

    private static final String APP_SHARED_PREFERENCES = "com.aeolus.pathfinder.model.preferences.app_shared_preferences";

    Context context;

    public PreferencesManagerImpl(Context context) {
        this.context = context;
    }

    @Override
    public void saveBoolean(String key, Boolean value) {
        context
                .getSharedPreferences(APP_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(key, value)
                .apply();
    }

    @Override
    public boolean getBoolean(String key) {
        return context
                .getSharedPreferences(APP_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getBoolean(key, false);
    }

    @Override
    public void saveString(String key, String value) {
        context
                .getSharedPreferences(APP_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply();
    }

    @Override
    public String getString(String key) {
        return context
                .getSharedPreferences(APP_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getString(key, "");
    }
}
