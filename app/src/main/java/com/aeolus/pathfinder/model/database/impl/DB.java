package com.aeolus.pathfinder.model.database.impl;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.aeolus.pathfinder.model.database.impl.dao.PopulatedPointDAO;
import com.aeolus.pathfinder.entities.PopulatedPoint;

/**
 * Created by Yuriy on 11.02.2018.
 */

@Database(entities = {PopulatedPoint.class}, version = 1)
public abstract class DB extends RoomDatabase {
    public abstract PopulatedPointDAO populatedPointDAO();
}
