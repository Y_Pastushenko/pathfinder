package com.aeolus.pathfinder.model.route;

import com.aeolus.pathfinder.entities.PopulatedPoint;

import java.util.ArrayList;

/**
 * Created by y_pastushenko on 27.03.18.
 */

public class LocationCalculator {

    public static double getDistance(PopulatedPoint pp1, PopulatedPoint pp2) {
        double lat1 = pp1.getLatitude();
        double lon1 = pp1.getLongitude();
        double lat2 = pp2.getLatitude();
        double lon2 = pp2.getLongitude();
        double el1 = 0;
        double el2 = 0;

        final int R = 6371; // Radius of the earth
        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters
        double height = el1 - el2;
        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        return Math.sqrt(distance);
    }

    public static PopulatedPoint getNearestBetween(PopulatedPoint pp1, PopulatedPoint pp2, ArrayList<PopulatedPoint> populatedPoints) {
        PopulatedPoint nearest = null;
        double distance = getDistance(pp1, pp2) * 1000;
        for (PopulatedPoint pp : populatedPoints) {
            double nDistance = (getDistance(pp, pp1) + getDistance(pp, pp2));
            if (nDistance < distance) {
                distance = getDistance(pp, pp1) + getDistance(pp, pp2);
                nearest = pp;
            }
        }
        return nearest;
    }

}
