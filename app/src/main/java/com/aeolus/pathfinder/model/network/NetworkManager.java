package com.aeolus.pathfinder.model.network;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;

import org.joda.time.LocalDate;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 26.03.2018.
 */

public interface NetworkManager {

    Observable<ArrayList<Transfer>> getTransfers(String departureKoatuu, String destinationKoatuu, LocalDate date);

    Observable<ArrayList<PopulatedPoint>> getPopulatedPoints();

    void clearGetBusesAsyncTasks();
}
