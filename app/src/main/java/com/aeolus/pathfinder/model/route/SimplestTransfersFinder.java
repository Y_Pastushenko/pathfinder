package com.aeolus.pathfinder.model.route;

import android.annotation.SuppressLint;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.model.database.DBManager;
import com.aeolus.pathfinder.model.network.NetworkManager;
import com.annimon.stream.Stream;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SimplestTransfersFinder {

    enum RouteType {
        OBLAST_CAP_to_OBLAST_CAP,
        OBLAST_CAP_to_RAION_CAP,
        OBLAST_CAP_to_OTHER,
        RAION_CAP_to_OBLAST_CAP,
        RAION_CAP_to_RAION_CAP,
        RAION_CAP_to_OTHER,
        OTHER_to_OBLAST_CAP,
        OTHER_to_RAION_CAP,
        OTHER_to_OTHER
    }

    private class NothingFound extends Exception {

    }

    private NetworkManager networkManager;

    private DBManager dbManager;

    private AtomicInteger threadsCount1;

    private AtomicInteger threadsCount2;

    public SimplestTransfersFinder(
            NetworkManager networkManager,
            DBManager dbManager) {
        this.networkManager = networkManager;
        this.dbManager = dbManager;
    }

//    @SuppressLint("CheckResult")
//    public void start() {
//        toPrecess.add(new Pair<>(departurePoint, destinationPoint));
//        capitals = dbManager.getPopulatedPointsByKoatuusImm(KOATUUResolver.getOblastCapitals());
//        capitals.remove(departurePoint);
//        capitals.remove(destinationPoint);
//        while (!toPrecess.isEmpty()) {
//            Pair<PopulatedPoint, PopulatedPoint> pair = toPrecess.get(0);
//            networkManager.getTransfers(departurePoint.getKoatuu(), destinationPoint.getKoatuu(), date).observeOn(AndroidSchedulers.mainThread()).subscribe(ts -> {
//                toPrecess.remove(pair);
//                if (ts.isEmpty()) {
//                    PopulatedPoint newPp = LocationCalculator.getNearestBetween(pair.first, pair.second, capitals);
//                    toPrecess.add(new Pair<>(departurePoint, newPp));
//                    toPrecess.add(new Pair<>(newPp, destinationPoint));
//                    capitals.remove(newPp);
//                } else
//                    transfers.addAll(ts);
//            });
//        }
//        listener.onTransfersGot(transfers);
//    }

    private RouteType getRouteType(ArrayList<PopulatedPoint> oblastCapitals, ArrayList<PopulatedPoint> raionCapitals, PopulatedPoint departurePoint, PopulatedPoint destinationPoint) {
        if (oblastCapitals.contains(departurePoint)) {
            if (oblastCapitals.contains(destinationPoint))
                return RouteType.OBLAST_CAP_to_OBLAST_CAP;
            if (raionCapitals.contains(destinationPoint))
                return RouteType.OBLAST_CAP_to_RAION_CAP;
            return RouteType.OBLAST_CAP_to_OTHER;
        }
        if (raionCapitals.contains(departurePoint)) {
            if (oblastCapitals.contains(destinationPoint))
                return RouteType.RAION_CAP_to_OBLAST_CAP;
            if (raionCapitals.contains(destinationPoint))
                return RouteType.RAION_CAP_to_RAION_CAP;
            return RouteType.RAION_CAP_to_OTHER;
        }
        if (oblastCapitals.contains(destinationPoint))
            return RouteType.OTHER_to_OBLAST_CAP;
        if (raionCapitals.contains(destinationPoint))
            return RouteType.OTHER_to_RAION_CAP;
        return RouteType.OTHER_to_OTHER;
    }

    @SuppressLint("CheckResult")
    public Observable<ArrayList<Transfer>> getTransfers(PopulatedPoint departurePoint, PopulatedPoint destinationPoint, LocalDate date) {
        Observable observable = Observable.create(emitter ->
                dbManager
                        .getPopulatedPointsByKoatuus(KOATUUResolver.getRaionCapitals())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(raionCapitals ->
                                dbManager
                                        .getPopulatedPointsByKoatuus(KOATUUResolver.getOblastCapitals())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(oblastCapitals -> {
                                            networkManager.getTransfers(departurePoint.getKoatuu(), destinationPoint.getKoatuu(), date).subscribe(ts -> {
                                                if (!ts.isEmpty()) {
                                                    emitter.onNext(ts);
//                                                    emitter.onComplete();
                                                }
                                            });
                                            Observer<ArrayList<Transfer>> observer;
                                            switch (getRouteType(oblastCapitals, raionCapitals, departurePoint, destinationPoint)) {
                                                case OBLAST_CAP_to_OBLAST_CAP:
                                                    observer = new Observer<ArrayList<Transfer>>() {
                                                        @Override
                                                        public void onSubscribe(Disposable d) {

                                                        }

                                                        @Override
                                                        public void onNext(ArrayList<Transfer> transfers) {
                                                            if (!transfers.isEmpty())
                                                                emitter.onNext(transfers);
                                                        }

                                                        @Override
                                                        public void onError(Throwable e) {
                                                            emitter.onError(e);
                                                        }

                                                        @Override
                                                        public void onComplete() {
                                                            emitter.onComplete();
                                                        }
                                                    };
                                                    getTransfersBetweenCapitals(departurePoint, destinationPoint, date).subscribe(observer);
                                                    break;
                                                case OBLAST_CAP_to_RAION_CAP:
                                                    if (KOATUUResolver.getOblastCapital(destinationPoint.getKoatuu()).equals(departurePoint.getKoatuu())) {
                                                        observer = new Observer<ArrayList<Transfer>>() {
                                                            @Override
                                                            public void onSubscribe(Disposable d) {

                                                            }

                                                            @Override
                                                            public void onNext(ArrayList<Transfer> transfers) {
                                                                if (!transfers.isEmpty())
                                                                    emitter.onNext(transfers);
                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {
                                                                emitter.onError(e);
                                                            }

                                                            @Override
                                                            public void onComplete() {
                                                                emitter.onComplete();
                                                            }
                                                        };
                                                        networkManager
                                                                .getTransfers(departurePoint.getKoatuu(), destinationPoint.getKoatuu(), date)
                                                                .subscribe(observer);
                                                    } else {
                                                        observer = new Observer<ArrayList<Transfer>>() {
                                                            @Override
                                                            public void onSubscribe(Disposable d) {

                                                            }

                                                            @Override
                                                            public void onNext(ArrayList<Transfer> transfers) {
                                                                if (transfers.isEmpty()) {
                                                                    emitter.onError(new NothingFound());
                                                                    return;
                                                                }
                                                                dbManager
                                                                        .getPopulatedPoint(KOATUUResolver.getOblastCapital(destinationPoint.getKoatuu()))
                                                                        .observeOn(AndroidSchedulers.mainThread())
                                                                        .subscribe(secondCap -> dbManager
                                                                                .getPopulatedPoint(departurePoint.getKoatuu())
                                                                                .subscribe(firstCap ->
                                                                                        getTransfersBetweenCapitals(firstCap, secondCap, date)
                                                                                                .subscribe(tBC -> {
                                                                                                    if (tBC.isEmpty()) {
                                                                                                        emitter.onError(new NothingFound());
//                                                                                                            emitter.onComplete();
                                                                                                        return;
                                                                                                    }
                                                                                                    ArrayList<Transfer> result = new ArrayList<>();
                                                                                                    result.addAll(transfers);
                                                                                                    result.addAll(tBC);
                                                                                                    emitter.onNext(result);
                                                                                                    return;
                                                                                                })));
                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {
                                                                emitter.onError(e);
                                                            }

                                                            @Override
                                                            public void onComplete() {
                                                                emitter.onComplete();
                                                            }
                                                        };
                                                        networkManager
                                                                .getTransfers(KOATUUResolver.getOblastCapital(destinationPoint.getKoatuu()), destinationPoint.getKoatuu(), date)
                                                                .subscribe(observer);
                                                    }
                                                    break;
                                                case OBLAST_CAP_to_OTHER:
                                                    break;
                                                case RAION_CAP_to_OBLAST_CAP:
                                                    if (KOATUUResolver.getOblastCapital(departurePoint.getKoatuu()).equals(destinationPoint.getKoatuu())) {
                                                        observer = new Observer<ArrayList<Transfer>>() {
                                                            @Override
                                                            public void onSubscribe(Disposable d) {

                                                            }

                                                            @Override
                                                            public void onNext(ArrayList<Transfer> transfers) {
                                                                if (transfers.isEmpty()) {
                                                                    emitter.onError(new NothingFound());
                                                                    emitter.onComplete();
                                                                    return;
                                                                }
                                                                emitter.onNext(transfers);
                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {
                                                                emitter.onError(e);
                                                            }

                                                            @Override
                                                            public void onComplete() {
                                                                emitter.onComplete();
                                                            }
                                                        };
                                                        networkManager
                                                                .getTransfers(departurePoint.getKoatuu(), destinationPoint.getKoatuu(), date)
                                                                .subscribe(observer);
                                                    } else {
                                                        observer = new Observer<ArrayList<Transfer>>() {
                                                            @Override
                                                            public void onSubscribe(Disposable d) {

                                                            }

                                                            @Override
                                                            public void onNext(ArrayList<Transfer> transfers) {
                                                                if (transfers.isEmpty()) {
                                                                    emitter.onError(new NothingFound());
                                                                    emitter.onComplete();
                                                                    return;
                                                                }
                                                                dbManager
                                                                        .getPopulatedPoint(KOATUUResolver.getOblastCapital(departurePoint.getKoatuu()))
                                                                        .observeOn(AndroidSchedulers.mainThread())
                                                                        .subscribe(firstCap -> dbManager
                                                                                .getPopulatedPoint(destinationPoint.getKoatuu())
                                                                                .subscribe(secondCap -> {
                                                                                    getTransfersBetweenCapitals(firstCap, secondCap, date)
                                                                                            .subscribe(tBC -> {
                                                                                                if (tBC.isEmpty()) {
                                                                                                    emitter.onError(new NothingFound());
                                                                                                    emitter.onComplete();
                                                                                                    return;
                                                                                                }
                                                                                                ArrayList<Transfer> result = new ArrayList<>();
                                                                                                result.addAll(transfers);
                                                                                                result.addAll(tBC);
                                                                                                emitter.onNext(result);
//                                                                                        emitter.onComplete();
                                                                                                return;
                                                                                            });
                                                                                }));
                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {
                                                                emitter.onError(e);
                                                            }

                                                            @Override
                                                            public void onComplete() {
                                                                emitter.onComplete();
                                                            }
                                                        };
                                                        networkManager
                                                                .getTransfers(departurePoint.getKoatuu(), KOATUUResolver.getOblastCapital(departurePoint.getKoatuu()), date)
                                                                .subscribe(observer);
                                                    }
                                                    break;
                                                case RAION_CAP_to_RAION_CAP:
                                                    if (KOATUUResolver.fromOneOblast(departurePoint.getKoatuu(), destinationPoint.getKoatuu())) {
                                                        observer = new Observer<ArrayList<Transfer>>() {
                                                            @Override
                                                            public void onSubscribe(Disposable d) {

                                                            }

                                                            @Override
                                                            public void onNext(ArrayList<Transfer> firstToCap) {
                                                                if (firstToCap.isEmpty()) {
                                                                    emitter.onError(new NothingFound());
                                                                    emitter.onComplete();
                                                                    return;
                                                                }
                                                                networkManager
                                                                        .getTransfers(KOATUUResolver.getOblastCapital(destinationPoint.getKoatuu()), destinationPoint.getKoatuu(), date)
                                                                        .subscribe(capToSec -> {
                                                                            if (capToSec.isEmpty()) {
                                                                                emitter.onError(new NothingFound());
                                                                                emitter.onComplete();
                                                                                return;
                                                                            }
                                                                            ArrayList<Transfer> result = new ArrayList<>();
                                                                            result.addAll(firstToCap);
                                                                            result.addAll(capToSec);
                                                                            emitter.onNext(result);
                                                                            return;
                                                                        });
                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {
                                                                emitter.onError(e);
                                                            }

                                                            @Override
                                                            public void onComplete() {
                                                                emitter.onComplete();
                                                            }
                                                        };
                                                        networkManager
                                                                .getTransfers(departurePoint.getKoatuu(), KOATUUResolver.getOblastCapital(departurePoint.getKoatuu()), date)
                                                                .subscribe(observer);
                                                    } else {
                                                        observer = new Observer<ArrayList<Transfer>>() {
                                                            @Override
                                                            public void onSubscribe(Disposable d) {

                                                            }

                                                            @Override
                                                            public void onNext(ArrayList<Transfer> firstToCap) {
                                                                if (firstToCap.isEmpty()) {
                                                                    emitter.onError(new NothingFound());
                                                                    emitter.onComplete();
                                                                    return;
                                                                }
                                                                networkManager
                                                                        .getTransfers(KOATUUResolver.getOblastCapital(destinationPoint.getKoatuu()), destinationPoint.getKoatuu(), date)
                                                                        .subscribe(capToSec -> {
                                                                            if (capToSec.isEmpty()) {
                                                                                emitter.onError(new NothingFound());
                                                                                emitter.onComplete();
                                                                                return;
                                                                            }
                                                                            dbManager
                                                                                    .getPopulatedPoint(KOATUUResolver.getOblastCapital(departurePoint.getKoatuu()))
                                                                                    .observeOn(AndroidSchedulers.mainThread())
                                                                                    .subscribe(firstCap -> dbManager
                                                                                            .getPopulatedPoint(KOATUUResolver.getOblastCapital(destinationPoint.getKoatuu()))
                                                                                            .subscribe(secondCap -> getTransfersBetweenCapitals(firstCap, secondCap, date)
                                                                                                    .subscribe(transfersBetweenCapitals -> {
                                                                                                        if (transfersBetweenCapitals.isEmpty()) {
                                                                                                            emitter.onError(new NothingFound());
                                                                                                            emitter.onComplete();
                                                                                                            return;
                                                                                                        }
                                                                                                        ArrayList<Transfer> result = new ArrayList<>();
                                                                                                        result.addAll(firstToCap);
                                                                                                        result.addAll(capToSec);
                                                                                                        result.addAll(transfersBetweenCapitals);
                                                                                                        emitter.onNext(result);
//                                                                                                emitter.onComplete();
                                                                                                        return;
                                                                                                    })));
                                                                        });
                                                            }

                                                            @Override
                                                            public void onError(Throwable e) {
                                                                emitter.onError(e);
                                                            }

                                                            @Override
                                                            public void onComplete() {
                                                                emitter.onComplete();
                                                            }
                                                        };
                                                        networkManager
                                                                .getTransfers(departurePoint.getKoatuu(), KOATUUResolver.getOblastCapital(departurePoint.getKoatuu()), date)
                                                                .subscribe(observer);
                                                    }
                                                    break;
                                                case RAION_CAP_to_OTHER:
                                                    break;
                                                case OTHER_to_OBLAST_CAP:
                                                    break;
                                                case OTHER_to_RAION_CAP:
                                                    break;
                                                case OTHER_to_OTHER:
                                                    break;
                                            }
                                        })));
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    public Observable<ArrayList<Transfer>> getTransfersBetweenCapitals(PopulatedPoint departurePoint, PopulatedPoint destinationPoint, LocalDate date) {
        @SuppressLint("CheckResult") Observable observable = Observable.create(emitter ->
                dbManager.getPopulatedPointsByKoatuus(KOATUUResolver.getOblastCapitals()).observeOn(AndroidSchedulers.mainThread()).subscribe(oblastCapitals ->
                        networkManager.getTransfers(departurePoint.getKoatuu(), destinationPoint.getKoatuu(), date).observeOn(AndroidSchedulers.mainThread()).subscribe(transfers -> {
                            if (!transfers.isEmpty()) {
                                emitter.onNext(transfers);
//                                emitter.onComplete();
//                                return;
                            }
                            ArrayList<PopulatedPoint> capitals = oblastCapitals;
                            capitals.remove(departurePoint);
                            capitals.remove(destinationPoint);
                            capitals = new ArrayList<>(Stream.of(capitals).sorted((o1, o2) -> {
                                double v1 = LocationCalculator.getDistance(departurePoint, o1) + LocationCalculator.getDistance(o1, destinationPoint);
                                double v2 = LocationCalculator.getDistance(departurePoint, o2) + LocationCalculator.getDistance(o2, destinationPoint);
                                if (v1 > v2) return 1;
                                else return -1;
                            }).toList());
                            ArrayList<PopulatedPoint> selected = new ArrayList<>(capitals.subList(0, Math.round((float) capitals.size() / 2f)));
                            threadsCount1 = new AtomicInteger();
                            threadsCount2 = new AtomicInteger();
                            threadsCount1.set(0);
                            threadsCount2.set(0);
                            threadsCount1.set(selected.size());
                            for (PopulatedPoint capital : selected) {
                                networkManager.getTransfers(departurePoint.getKoatuu(), capital.getKoatuu(), date).observeOn(AndroidSchedulers.mainThread()).subscribe(transfers1 -> {
                                    threadsCount1.decrementAndGet();
                                    if (!transfers1.isEmpty()) {
                                        threadsCount2.incrementAndGet();
                                        networkManager
                                                .getTransfers(capital.getKoatuu(), destinationPoint.getKoatuu(), date)
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(transfers2 -> {
                                                    threadsCount2.decrementAndGet();
                                                    if (!transfers2.isEmpty()) {
                                                        ArrayList<Transfer> result = new ArrayList<>();
                                                        result.addAll(transfers1);
                                                        result.addAll(transfers2);
                                                        emitter.onNext(result);
//                                        emitter.onComplete();
                                                    }
                                                    if (threadsCount1.get() == 0 && threadsCount2.get() == 0)
                                                        emitter.onComplete();
                                                });
                                    }
                                    if (threadsCount1.get() == 0 && threadsCount2.get() == 0)
                                        emitter.onComplete();
                                });
                            }
                        })));
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    public void cancel() {
        networkManager.clearGetBusesAsyncTasks();
    }
}
