package com.aeolus.pathfinder.model.database.impl.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.model.database.impl.DBSchema;

import java.util.List;

import io.reactivex.Maybe;

/**
 * Created by Yuriy on 11.02.2018.
 */

@Dao
public interface PopulatedPointDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(PopulatedPoint... populatedPoint);

    @Query("SELECT * FROM " +
            DBSchema.PopulatedPointsTable.NAME +
            " WHERE " + DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_KOATUU + " = :koatuu")
    Maybe<PopulatedPoint> getPopulatedPointById(String koatuu);

    @Query("SELECT * FROM " +
            DBSchema.PopulatedPointsTable.NAME +
            " WHERE " + DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_KOATUU + " = :koatuu")
    PopulatedPoint getPopulatedPointByIdImm(String koatuu);

    @Query("SELECT * FROM " + DBSchema.PopulatedPointsTable.NAME)
    Maybe<List<PopulatedPoint>> getAllPopulatedPoints();

    @Query("SELECT * FROM " + DBSchema.PopulatedPointsTable.NAME)
    List<PopulatedPoint> getAllPopulatedPointsImm();

    @Query("SELECT * FROM " + DBSchema.PopulatedPointsTable.NAME + " WHERE " + DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_NAME + " = :name")
    Maybe<List<PopulatedPoint>> getPopulatedPointsByName(String name);

    @Query("SELECT * FROM " + DBSchema.PopulatedPointsTable.NAME + " WHERE " + DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_NAME + " = :name")
    List<PopulatedPoint> getPopulatedPointsByNameImm(String name);

    @Query("SELECT * FROM " + DBSchema.PopulatedPointsTable.NAME + " WHERE " + DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_NAME + " LIKE " + ":prefix")
    Maybe<List<PopulatedPoint>> getPopulatedPointsBeginningWith(String prefix);

    @Query("SELECT * FROM " + DBSchema.PopulatedPointsTable.NAME + " WHERE " + DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_NAME + " LIKE " + ":prefix")
    List<PopulatedPoint> getPopulatedPointsBeginningWithImm(String prefix);

}
