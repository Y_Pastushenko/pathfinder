package com.aeolus.pathfinder.model.route;

import android.util.Pair;

import java.util.ArrayList;

public class Dijkstra {

    public interface DataProvider {

        <V, E> V getDestinationVertex(E edge);

        <V, E> ArrayList<E> getEdges(V vertex);

        <E> double getPrice(E edge);

        <V> boolean vertexEquals(V vertex1, V vertex2);
    }

    public interface DijkstraEventsListener {

        <E> void onAppendEdgeToRoutes(E edge);

        <E> void onOptimalRouteFound(ArrayList<E> route, double price);

        void onNoAvailablePathFound();
    }

    public <V, E> void getShortestPath(V departurePoint, V destinationPoint, DataProvider dataProvider, DijkstraEventsListener dijkstraProgressListener) {
        ArrayList<V> seenVertices = new ArrayList<>();                                 //список вершин, до которых уже проложен кратчайший путь
        ArrayList<Pair<ArrayList<E>, Double>> selectedRoutes = new ArrayList<>();                  //список всех возможных маршрутов (и их частей)(в виде списка граней) и их стоимостей
        seenVertices.add(departurePoint);                                                   //добавляем в список вершин, через которые уэе проложен кратчайший путь т. отправления
        ArrayList<E> edges;
        Pair<ArrayList<E>, Double> localOptimalOption;                      //оптимальный вариант для этой вершины
        boolean allBeginningEdgesUsed = false;
        while (true) {                                                                         //для всего графа ищем пока не уткнемся в нужную и выкинем брейк
            Pair<ArrayList<E>, Double> globalOptimalOption = null;                       //текущий оптимальный вариант для всех вершин

            //для вершины отправления (т.к. сохраняем в routes грани, которые указывают на следующую вершину, а на первую ничто не указывает

            localOptimalOption = null;
            if (!allBeginningEdgesUsed) {
                edges = dataProvider.getEdges(departurePoint);                                     //берем для точки отправления все исходящие грани
                //оптимальный вариант для этой вершины
                for (E edge : edges) {                                                     //ищем  оптимальный вариант для этой  вершины
                    if (vertexContains(seenVertices, dataProvider.getDestinationVertex(edge), dataProvider))          //если грань указывает на эту же вершину - пропускаем
                        continue;
                    if (localOptimalOption == null) {                                           //если первая из просмотренных граней, то приравниваем localOptimalOption
                        ArrayList<E> localOptimalOptionRoute = new ArrayList<>();
                        localOptimalOptionRoute.add(edge);
                        localOptimalOption = new Pair<>(localOptimalOptionRoute, dataProvider.getPrice(edge));
                    } else {                                                                    //если не первая, то приравниваем localOptimalOption если у той цена больше
                        Double price = dataProvider.getPrice(edge);
                        if (price >= localOptimalOption.second)
                            continue;
                        ArrayList<E> localOptimalOptionRoute = new ArrayList<>();
                        localOptimalOptionRoute.add(edge);
                        localOptimalOption = new Pair<>(localOptimalOptionRoute, price);
                    }
                }
                if (localOptimalOption != null)
                    globalOptimalOption = localOptimalOption;
                else
                    allBeginningEdgesUsed = true;
            }

            // для остальных
            for (Pair<ArrayList<E>, Double> pair : selectedRoutes) {                          //Для каждого существующего варианта пути
                ArrayList<E> currentRoute = pair.first;
                Double currentPrice = pair.second;
                V supremeVertex = dataProvider.getDestinationVertex(currentRoute.get(currentRoute.size() - 1));           //берем крайнюю вершину
                if (dataProvider.getEdges(supremeVertex).isEmpty())                                  //если она тупиковая - то на следующую итерацию
                    continue;
                edges = dataProvider.getEdges(supremeVertex);                                        //берем для нее все исходящие грани                 //опотимальный вариант для этой вершины
                for (E edge : edges) {                                                          //ищем самый дешевый вариант для этой карйней вершины
                    if (vertexContains(seenVertices, dataProvider.getDestinationVertex(edge), dataProvider))              // если грань указывает на першину, через которую уже проложен кратчайший путь, то на следующую итерацию
                        continue;
                    if (localOptimalOption == null) {
                        ArrayList<E> localOptimalOptionRoute = new ArrayList<>(currentRoute);
                        localOptimalOptionRoute.add(edge);
                        localOptimalOption = new Pair<>(localOptimalOptionRoute, currentPrice + dataProvider.getPrice(edge));
                    } else {
                        Double price = currentPrice + dataProvider.getPrice(edge);
                        if (price >= localOptimalOption.second)
                            continue;
                        ArrayList<E> localOptimalOptionRoute = new ArrayList<>(currentRoute);
                        localOptimalOptionRoute.add(edge);
                        localOptimalOption = new Pair<>(localOptimalOptionRoute, price);
                    }
                }
                if (globalOptimalOption == null || globalOptimalOption.second > localOptimalOption.second)
                    globalOptimalOption = localOptimalOption;
            }
            if (globalOptimalOption == null) {
                dijkstraProgressListener.onNoAvailablePathFound();
                return;
            }
            if (dataProvider.vertexEquals(dataProvider.getDestinationVertex(globalOptimalOption.first.get(globalOptimalOption.first.size() - 1)), destinationPoint)) {
                dijkstraProgressListener.onOptimalRouteFound(globalOptimalOption.first, globalOptimalOption.second);
                return;
            }
            seenVertices.add(dataProvider.getDestinationVertex(globalOptimalOption.first.get(globalOptimalOption.first.size() - 1)));
            selectedRoutes.add(globalOptimalOption);
            dijkstraProgressListener.onAppendEdgeToRoutes(globalOptimalOption.first.get(globalOptimalOption.first.size() - 1));
        }
    }

    private <V> boolean vertexContains(ArrayList<V> list, V obj, DataProvider dataProvider) {
        for (V v : list)
            if (dataProvider.vertexEquals(v, obj))
                return true;
        return false;
    }
}
