package com.aeolus.pathfinder.model.database;

import com.aeolus.pathfinder.entities.PopulatedPoint;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;

/**
 * Created by Yuriy on 26.03.2018.
 */

public interface DBManager {

    Observable<Boolean> init();

    Observable<Integer> inflatePopulatedPoints(ArrayList<PopulatedPoint> populatedPoints);

    Maybe<PopulatedPoint> getPopulatedPoint(String koatuu);

    PopulatedPoint getPopulatedPointImm(String koatuu);

    Maybe<List<PopulatedPoint>> getAllPopulatedPoints();

    List<PopulatedPoint> getAllPopulatedPointsImm();

    Maybe<List<PopulatedPoint>> getPopulatedPointsByName(String name);

    List<PopulatedPoint> getPopulatedPointsByNameImm(String name);

    Maybe<List<PopulatedPoint>> getPopulatedPointsBeginningWith(String prefix);

    List<PopulatedPoint> getPopulatedPointsBeginningWithImm(String prefix);

    Observable<ArrayList<PopulatedPoint>> getPopulatedPointsByKoatuus(ArrayList<String> koatuus);

    ArrayList<PopulatedPoint> getPopulatedPointsByKoatuusImm(ArrayList<String> koatuus);

    void savePopulatedPoint(PopulatedPoint populatedPoint);


}
