package com.aeolus.pathfinder.model.preferences;

public interface PreferencesManager {

    void saveString(String key, String value);

    String getString(String key);

    void saveBoolean(String key, Boolean value);

    boolean getBoolean(String key);

}
