package com.aeolus.pathfinder.model.network;

import android.content.Context;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.model.network.impl.GetBusesAsyncTask;
import com.aeolus.pathfinder.model.network.impl.GetPopulatedPointsTask;

import org.joda.time.LocalDate;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by Yuriy on 26.03.2018.
 */

public class NetworkManagerImpl implements NetworkManager {

    private Context context;

    private ArrayList<GetBusesAsyncTask> busesAsyncTasks;

    public NetworkManagerImpl(Context context) {
        this.context = context;
        busesAsyncTasks = new ArrayList<>();
    }

    public Observable<ArrayList<Transfer>> getTransfers(String departureKoatuu, String destinationKoatuu, LocalDate date) {
        ArrayList<GetBusesAsyncTask> actual = new ArrayList<>();
        for (GetBusesAsyncTask asyncTask : busesAsyncTasks)
            if (asyncTask != null && !asyncTask.isCancelled())
                actual.add(asyncTask);
        busesAsyncTasks = new ArrayList<>(actual);
        GetBusesAsyncTask busesAsyncTask = new GetBusesAsyncTask();
        busesAsyncTasks.add(busesAsyncTask);
        return busesAsyncTask.getTransfers(context, departureKoatuu, destinationKoatuu, date);
    }


    public Observable<ArrayList<PopulatedPoint>> getPopulatedPoints() {
        GetPopulatedPointsTask populatedPointsTask = new GetPopulatedPointsTask();
        return populatedPointsTask.getPopulatedPoints(context);
    }

    public void clearGetBusesAsyncTasks() {
        for (GetBusesAsyncTask asyncTask : busesAsyncTasks)
            if (asyncTask != null)
                asyncTask.cancel(true);
        busesAsyncTasks.clear();
    }
}
