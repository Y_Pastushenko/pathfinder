package com.aeolus.pathfinder.model.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.model.database.impl.DB;
import com.aeolus.pathfinder.model.database.impl.DBSchema;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Yuriy on 11.02.2018.
 */

public class DBManagerImpl implements DBManager {

    private DB db;
    private Context context;
    private boolean dbInflated = true;
    private int loadingProgress;

    public DBManagerImpl(Context context) {
        this.context = context;
    }

    public Observable<Boolean> init() {
        Observable<Boolean> observable;
        observable = Observable.create(emitter -> {
            RoomDatabase.Callback rdc = new RoomDatabase.Callback() {

                public void onCreate(SupportSQLiteDatabase sdb) {
                }

                public void onOpen(SupportSQLiteDatabase db) {
                    emitter.onNext(true);
                    emitter.onComplete();
                }
            };
            db = Room.databaseBuilder(context, DB.class, DBSchema.NAME).addCallback(rdc).build();
            db.getOpenHelper().getReadableDatabase();
        });
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    public Observable<Integer> inflatePopulatedPoints(ArrayList<PopulatedPoint> populatedPoints) {
        Observable<Integer> observable;
        observable = Observable.create(emitter -> {
            loadingProgress = 0;
            Observable.just(db).subscribeOn(Schedulers.io()).subscribe(db -> {
                for (int i = 0; i < populatedPoints.size(); i++) {
                    db.populatedPointDAO().insert(populatedPoints.get(i));
                    int np = Math.round((((float) i) / ((float) populatedPoints.size())) * 100);
                    if (loadingProgress != np)
                        emitter.onNext(loadingProgress = np);
//                    Log.e("lolkalolk", "" + i);
                }
                emitter.onComplete();
            });
        });
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    public void savePopulatedPoint(PopulatedPoint populatedPoint) {
        Observable.just(db)
                .subscribeOn(Schedulers.io())
                .subscribe(db -> db.populatedPointDAO().insert(populatedPoint));
    }

    public Maybe<PopulatedPoint> getPopulatedPoint(String koatuu) {
        return db.populatedPointDAO().getPopulatedPointById(koatuu).subscribeOn(Schedulers.io());
    }

    public PopulatedPoint getPopulatedPointImm(String koatuu) {
        return db.populatedPointDAO().getPopulatedPointByIdImm(koatuu);
    }

    public Maybe<List<PopulatedPoint>> getAllPopulatedPoints() {
        return db.populatedPointDAO().getAllPopulatedPoints().subscribeOn(Schedulers.io());
    }

    public List<PopulatedPoint> getAllPopulatedPointsImm() {
        return db.populatedPointDAO().getAllPopulatedPointsImm();
    }

    public Maybe<List<PopulatedPoint>> getPopulatedPointsByName(String name) {
        return db.populatedPointDAO().getPopulatedPointsByName(name).subscribeOn(Schedulers.io());
    }

    public List<PopulatedPoint> getPopulatedPointsByNameImm(String name) {
        return db.populatedPointDAO().getPopulatedPointsByNameImm(name);
    }

    public Maybe<List<PopulatedPoint>> getPopulatedPointsBeginningWith(String prefix) {
        return db.populatedPointDAO().getPopulatedPointsBeginningWith(prefix + "%").subscribeOn(Schedulers.io());
    }

    public List<PopulatedPoint> getPopulatedPointsBeginningWithImm(String prefix) {
        return db.populatedPointDAO().getPopulatedPointsBeginningWithImm(prefix + "%");
    }

    public Observable<ArrayList<PopulatedPoint>> getPopulatedPointsByKoatuus(ArrayList<String> koatuus) {
        Observable<ArrayList<PopulatedPoint>> observable = Observable.create(emiiter -> {
            Observable.just(db).subscribeOn(Schedulers.io()).subscribe(db -> {
                ArrayList<PopulatedPoint> populatedPoints = new ArrayList<>();
                for (String koatuu : koatuus)
                    populatedPoints.add(db.populatedPointDAO().getPopulatedPointByIdImm(koatuu));
                emiiter.onNext(populatedPoints);
                emiiter.onComplete();
            });
        });
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    public ArrayList<PopulatedPoint> getPopulatedPointsByKoatuusImm(ArrayList<String> koatuus) {
        ArrayList<PopulatedPoint> populatedPoints = new ArrayList<>();
        for (String koatuu : koatuus)
            populatedPoints.add(getPopulatedPointImm(koatuu));
        return populatedPoints;
    }
}
