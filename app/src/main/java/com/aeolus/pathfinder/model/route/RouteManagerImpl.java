package com.aeolus.pathfinder.model.route;

import android.annotation.SuppressLint;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.model.database.DBManager;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by Yuriy on 26.03.2018.
 */

public class RouteManagerImpl implements RouteManager {

    public enum SearchPriority {CHEAPEST, FASTEST}

    private SimplestTransfersFinder simplestTransfersFinder;

    private DBManager dbManager;

    private ArrayList<ArrayList<Transfer>> shownRoutes;

    private ArrayList<Transfer> transfers;

    private DisposableObserver<ArrayList<Transfer>> observer;

    public RouteManagerImpl(SimplestTransfersFinder simplestTransfersFinder, DBManager dbManager) {
        this.simplestTransfersFinder = simplestTransfersFinder;
        this.dbManager = dbManager;
    }

    @SuppressLint("CheckResult")
    public void search(PopulatedPoint departure, PopulatedPoint destination, DateTime from, SearchPriority priotity, SearchEventsListener listener) {
        transfers = new ArrayList<>();
        shownRoutes = new ArrayList<>();
        observer = new DisposableObserver<ArrayList<Transfer>>() {

            @Override
            public void onNext(ArrayList<Transfer> newTransfers) {
                transfers.addAll(newTransfers);
                if (transfers.isEmpty())
                    return;
                if (transfers.size() == 1) {
                    if (!shownRoutes.contains(transfers)) {
                        listener.onNewRouteFound(new ArrayList<>(transfers), transfers.get(0).getPrice());
                        shownRoutes.add(new ArrayList<>(transfers));
                    }
                    return;
                }
                dbManager
                        .getAllPopulatedPoints()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(populatedPoints -> {
                            try {
                                Graph graph = new Graph();
                                graph.inflate(new ArrayList<>(populatedPoints), transfers, () -> {
                                    PopulatedPoint resolvedDep;
                                    switch (priotity) {
                                        case CHEAPEST:
                                            android.util.Pair<Graph, HashMap<String, String>> cheapestResolvedGraphPair = graph.generateTimeResolvedGraph();
                                            Graph cheapestResolvedGraph = cheapestResolvedGraphPair.first;
                                            HashMap<String, String> cheapestIdsMapping = cheapestResolvedGraphPair.second;
                                            resolvedDep = cheapestResolvedGraph.getVertexWithTransfers(departure.getKoatuu(), from).getPopulatedPoint();
                                            Graph cheapestCleanedGraph = cheapestResolvedGraph.generateCleanedGraph(resolvedDep, destination);
                                            if (resolvedDep == null) {
                                                listener.onNoAvailablePathFound();
                                                return;
                                            }
                                            new Dijkstra()
                                                    .getShortestPath(
                                                            resolvedDep,
                                                            destination,
                                                            new Dijkstra.DataProvider() {
                                                                @Override
                                                                public <V, E> V getDestinationVertex(E edge) {
                                                                    return (V) cheapestCleanedGraph.getDestination((Transfer) edge);
                                                                }

                                                                @Override
                                                                public <V, E> ArrayList<E> getEdges(V vertex) {
                                                                    return (ArrayList<E>) cheapestCleanedGraph.getTransfersFrom((PopulatedPoint) vertex);
                                                                }

                                                                @Override
                                                                public <E> double getPrice(E edge) {
                                                                    return ((Transfer) edge).getPrice();
                                                                }

                                                                @Override
                                                                public <V> boolean vertexEquals(V vertex1, V vertex2) {
                                                                    return (vertex1).equals(vertex2);
                                                                }
                                                            }, new Dijkstra.DijkstraEventsListener() {
                                                                @Override
                                                                public <E> void onAppendEdgeToRoutes(E edge) {
                                                                    Transfer transfer = ((Transfer) edge).clone();
                                                                    if (cheapestIdsMapping.containsKey(transfer.getDeparturePointKoatuu()))
                                                                        transfer.setDeparturePointKoatuu(cheapestIdsMapping.get(transfer.getDeparturePointKoatuu()));
                                                                    if (cheapestIdsMapping.containsKey(transfer.getDestinationPointKoatuu()))
                                                                        transfer.setDestinationPointKoatuu(cheapestIdsMapping.get(transfer.getDestinationPointKoatuu()));
                                                                    listener.onAppendEdgeToRoutes(transfer);
                                                                }

                                                                @Override
                                                                public <E> void onOptimalRouteFound(ArrayList<E> route, double price) {
                                                                    ArrayList<Transfer> result = new ArrayList<>();
                                                                    for (E t : route) {
                                                                        Transfer transfer = ((Transfer) t).clone();
                                                                        if (cheapestIdsMapping.containsKey(transfer.getDeparturePointKoatuu()))
                                                                            transfer.setDeparturePointKoatuu(cheapestIdsMapping.get(transfer.getDeparturePointKoatuu()));
                                                                        if (cheapestIdsMapping.containsKey(transfer.getDestinationPointKoatuu()))
                                                                            transfer.setDestinationPointKoatuu(cheapestIdsMapping.get(transfer.getDestinationPointKoatuu()));
                                                                        result.add(transfer);
                                                                    }
                                                                    if (!shownRoutes.contains(result)) {
                                                                        listener.onNewRouteFound(new ArrayList<>(result), price);
                                                                        shownRoutes.add(new ArrayList<>(result));
                                                                    }
                                                                }

                                                                @Override
                                                                public void onNoAvailablePathFound() {
                                                                    listener.onNoAvailablePathFound();
                                                                }
                                                            });
                                            break;
                                        case FASTEST:
                                            android.util.Pair<Graph, HashMap<String, String>> fastestResolvedGraphPair = graph.generateTimeResolvedGraph();
                                            Graph fastestResolvedGraph = fastestResolvedGraphPair.first;
                                            HashMap<String, String> fastestIdsMapping = fastestResolvedGraphPair.second;
                                            resolvedDep = fastestResolvedGraph.getVertexWithTransfers(departure.getKoatuu(), from).getPopulatedPoint();
                                            Graph fastestCleanedGraph = fastestResolvedGraph.generateCleanedGraph(resolvedDep, destination);
                                            ArrayList<Transfer> lastStepTransfers = fastestCleanedGraph.getTransfersTo(destination.getKoatuu());
                                            if (lastStepTransfers.isEmpty()) {
                                                listener.onNoAvailablePathFound();
                                                return;
                                            }
                                            if (resolvedDep == null) {
                                                listener.onNoAvailablePathFound();
                                                return;
                                            }
                                            new Dijkstra()
                                                    .getShortestPath(
                                                            resolvedDep,
                                                            destination,
                                                            new Dijkstra.DataProvider() {
                                                                @Override
                                                                public <V, E> V getDestinationVertex(E edge) {
                                                                    return (V) fastestCleanedGraph.getDestination((Transfer) edge);
                                                                }

                                                                @Override
                                                                public <V, E> ArrayList<E> getEdges(V vertex) {
                                                                    return (ArrayList<E>) fastestCleanedGraph.getTransfersFrom((PopulatedPoint) vertex);
                                                                }

                                                                @Override
                                                                public <E> double getPrice(E edge) {
                                                                    return ((Transfer) edge).getPrice();
                                                                }

                                                                @Override
                                                                public <V> boolean vertexEquals(V vertex1, V vertex2) {
                                                                    return (vertex1).equals(vertex2);
                                                                }
                                                            }, new Dijkstra.DijkstraEventsListener() {
                                                                @Override
                                                                public <E> void onAppendEdgeToRoutes(E edge) {
                                                                    Transfer transfer = ((Transfer) edge).clone();
                                                                    if (fastestIdsMapping.containsKey(transfer.getDeparturePointKoatuu()))
                                                                        transfer.setDeparturePointKoatuu(fastestIdsMapping.get(transfer.getDeparturePointKoatuu()));
                                                                    if (fastestIdsMapping.containsKey(transfer.getDestinationPointKoatuu()))
                                                                        transfer.setDestinationPointKoatuu(fastestIdsMapping.get(transfer.getDestinationPointKoatuu()));
                                                                    listener.onAppendEdgeToRoutes(transfer);
                                                                }

                                                                @Override
                                                                public <E> void onOptimalRouteFound(ArrayList<E> route, double price) {
                                                                    ArrayList<Transfer> result = new ArrayList<>();
                                                                    for (E t : route) {
                                                                        Transfer transfer = ((Transfer) t).clone();
                                                                        if (fastestIdsMapping.containsKey(transfer.getDeparturePointKoatuu()))
                                                                            transfer.setDeparturePointKoatuu(fastestIdsMapping.get(transfer.getDeparturePointKoatuu()));
                                                                        if (fastestIdsMapping.containsKey(transfer.getDestinationPointKoatuu()))
                                                                            transfer.setDestinationPointKoatuu(fastestIdsMapping.get(transfer.getDestinationPointKoatuu()));
                                                                        result.add(transfer);
                                                                    }
                                                                    if (!shownRoutes.contains(result)) {
                                                                        listener.onNewRouteFound(new ArrayList<>(result), price);
                                                                        shownRoutes.add(new ArrayList<>(result));
                                                                    }
                                                                }

                                                                @Override
                                                                public void onNoAvailablePathFound() {
                                                                    listener.onNoAvailablePathFound();
                                                                }
                                                            });
                                            break;
                                    }
                                });
                            } catch (NullPointerException e) {
                                return;
                            }
                        });
            }

            @Override
            public void onError(Throwable e) {
                listener.onNoAvailablePathFound();
            }

            @Override
            public void onComplete() {
                if (shownRoutes.isEmpty()) {
                    listener.onNoAvailablePathFound();
                    stopSearch();
                    return;
                }
                stopSearch();
                listener.onComplete();
            }
        };
        simplestTransfersFinder
                .getTransfers(departure, destination, from.toLocalDate())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public void stopSearch() {
        if (observer != null)
            observer.dispose();
        if (simplestTransfersFinder != null) {
            simplestTransfersFinder.cancel();
            simplestTransfersFinder = null;
        }
        if (shownRoutes != null)
            shownRoutes.clear();
        if (transfers != null)
            transfers.clear();
    }

    public ArrayList<ArrayList<Transfer>> getShownRoutes() {
        return shownRoutes;
    }
}
