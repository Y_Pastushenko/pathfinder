package com.aeolus.pathfinder.model.route;

import android.util.Pair;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.annimon.stream.Stream;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by y_pastushenko on 27.03.18.
 */

public class Graph {

    public interface InflatingListener {
        void onGraphReady();
    }

    public class Vertex {
        private PopulatedPoint populatedPoint;
        private ArrayList<Transfer> transfersFrom = new ArrayList<>();
        private ArrayList<Transfer> transfersTo = new ArrayList<>();

        public Vertex(PopulatedPoint populatedPoint) {
            this.populatedPoint = populatedPoint;
        }

        public PopulatedPoint getPopulatedPoint() {
            return populatedPoint;
        }

        public void setPopulatedPoint(PopulatedPoint populatedPoint) {
            this.populatedPoint = populatedPoint;
        }

        public ArrayList<Transfer> getTransfersFrom() {
            return transfersFrom;
        }

        public void setTransfersFrom(ArrayList<Transfer> transfersFrom) {
            this.transfersFrom = transfersFrom;
        }

        public ArrayList<Transfer> getTransfersTo() {
            return transfersTo;
        }

        public void setTransfersTo(ArrayList<Transfer> transfersTo) {
            this.transfersTo = transfersTo;
        }

        public void addTransferTo(Transfer transfer) {
            transfersTo.add(transfer);
        }

        public void addTransferFrom(Transfer transfer) {
            transfersFrom.add(transfer);
        }
    }

    private HashMap<String, Vertex> graph = new HashMap<>();
    private HashMap<String, PopulatedPoint> populatedPoints = new HashMap<>();
    private ArrayList<Transfer> transfers = new ArrayList<>();

    public void inflate(ArrayList<PopulatedPoint> populatedPointsList, ArrayList<Transfer> transfers, InflatingListener listener) {
        for (PopulatedPoint p : populatedPointsList) {
            graph.put(p.getKoatuu(), new Vertex(p));
            populatedPoints.put(p.getKoatuu(), p);
        }
        this.transfers.addAll(transfers);
        for (Transfer t : transfers) {
            addTransferFrom(t.getDeparturePointKoatuu(), t);
            addTransferTo(t.getDestinationPointKoatuu(), t);
        }
        if (listener != null)
            listener.onGraphReady();
    }

    public void addVertex(Vertex vertex) {
        graph.put(vertex.getPopulatedPoint().getKoatuu(), vertex);
        populatedPoints.put(vertex.getPopulatedPoint().getKoatuu(), vertex.getPopulatedPoint());
    }

    public void removeVertex(String key) {
        graph.remove(key);
        populatedPoints.remove(key);
    }

    public void removeTransfer(Transfer t) {
        transfers.remove(t);
        for (Vertex v : graph.values()) {
            v.getTransfersTo().remove(t);
            v.getTransfersFrom().remove(t);
        }
    }

    void addTransferTo(String koatuu, Transfer transfer) {
        Vertex vertex = graph.get(koatuu);
        vertex.addTransferTo(transfer);
        if (!transfers.contains(transfer))
            transfers.add(transfer);
        graph.put(koatuu, vertex);
        populatedPoints.put(koatuu, vertex.getPopulatedPoint());
    }

    void addTransferFrom(String koatuu, Transfer transfer) {
        Vertex vertex = graph.get(koatuu);
        vertex.addTransferFrom(transfer);
        if (!transfers.contains(transfer))
            transfers.add(transfer);
        graph.put(koatuu, vertex);
        populatedPoints.put(koatuu, vertex.getPopulatedPoint());
    }

    public ArrayList<Transfer> getTransfersFrom(String koatuu) {
        return graph.get(koatuu).getTransfersFrom();
    }

    public ArrayList<Transfer> getTransfersFrom(PopulatedPoint populatedPoint) {
        return graph.get(populatedPoint.getKoatuu()).getTransfersFrom();
    }

    public ArrayList<Transfer> getTransfersTo(String koatuu) {
        return graph.get(koatuu).getTransfersTo();
    }

    public ArrayList<Transfer> getTransfersTo(PopulatedPoint populatedPoint) {
        return graph.get(populatedPoint.getKoatuu()).getTransfersTo();
    }

    public ArrayList<PopulatedPoint> getDirectionsTo(String koatuu) {
        ArrayList<PopulatedPoint> populatedPoints = new ArrayList<>();
        for (Transfer t : getTransfersFrom(koatuu))
            populatedPoints.add(this.populatedPoints.get(t.getDestinationPointKoatuu()));
        return populatedPoints;
    }

    public ArrayList<PopulatedPoint> getDirectionsFrom(String koatuu) {
        ArrayList<PopulatedPoint> populatedPoints = new ArrayList<>();
        for (Transfer t : getTransfersTo(koatuu))
            populatedPoints.add(this.populatedPoints.get(t.getDeparturePointKoatuu()));
        return populatedPoints;
    }

    public PopulatedPoint getDestination(Transfer t) {
        return populatedPoints.get(t.getDestinationPointKoatuu());
    }

    public PopulatedPoint getDeparturePoint(Transfer t) {
        return populatedPoints.get(t.getDeparturePointKoatuu());
    }

    public int getVertexesCount() {
        return graph.size();
    }

    public int getTransfersCount() {
        return transfers.size();
    }

    public HashMap<String, PopulatedPoint> getPopulatedPoints() {
        return populatedPoints;
    }

    public Pair<Graph, HashMap<String, String>> generateTimeResolvedGraph() {
        Graph tempGraph = clone();
        Graph newGraph = new Graph();
        HashMap<String, String> keysMapping = new HashMap<>();
        ArrayList<Transfer> transfers = new ArrayList<>(tempGraph.transfers);
        HashMap<String, PopulatedPoint> populatedPoints = new HashMap<>(tempGraph.populatedPoints);
        for (PopulatedPoint p : populatedPoints.values()) {
            ArrayList<Transfer> transfersFrom = new ArrayList<>(getTransfersFrom(p.getKoatuu()));
            transfersFrom = new ArrayList<>(Stream.of(transfersFrom).sorted((o1, o2) -> Long.compare(o1.getDepartureDateTime(), o2.getDepartureDateTime())).toList());
            for (Transfer t : transfersFrom) {
                String newID = p.getKoatuu() + Long.toString(t.getDepartureDateTime());
                PopulatedPoint newPP = p.clone();
                newPP.setKoatuu(newID);
                Vertex vertex = new Vertex(newPP);
                newGraph.addVertex(vertex);
                keysMapping.put(newID, p.getKoatuu());
            }
            newGraph.addVertex(new Vertex(p));
        }
        for (Transfer t : transfers) {
            ArrayList<Vertex> destVertices = new ArrayList<>(Stream.of(newGraph.graph.values()).filter(pp -> pp.getPopulatedPoint().getKoatuu().startsWith(t.getDestinationPointKoatuu())).toList());
            Vertex root = null;
            for (Vertex v : destVertices) {
                if (v.getPopulatedPoint().getKoatuu().length() == 10) {
                    root = v;
                    destVertices.remove(v);
                    break;
                }
            }
            if (destVertices.size() > 1)
                destVertices = new ArrayList<>(Stream.of(destVertices).sorted((o1, o2) -> Long.compare(Long.parseLong(o1.getPopulatedPoint().getKoatuu().substring(10)), Long.parseLong(o2.getPopulatedPoint().getKoatuu().substring(10)))).toList());
            boolean assigned = false;
            for (Vertex v : destVertices)
                if ((t.getArrivalDateTime() < Long.parseLong(v.getPopulatedPoint().getKoatuu().substring(10)))) {
                    t.setDestinationPointKoatuu(v.getPopulatedPoint().getKoatuu());
                    assigned = true;
                    break;
                }
            if (!assigned)
                t.setDestinationPointKoatuu(root.getPopulatedPoint().getKoatuu());
            newGraph.graph.get(t.getDestinationPointKoatuu()).addTransferTo(t);

            ArrayList<Vertex> depVertices = new ArrayList<>(Stream.of(newGraph.graph.values()).filter(pp -> pp.getPopulatedPoint().getKoatuu().startsWith(t.getDeparturePointKoatuu())).toList());
            for (Vertex v : depVertices) {
                if (v.getPopulatedPoint().getKoatuu().length() > 10 && t.getDepartureDateTime() >= Long.parseLong(v.getPopulatedPoint().getKoatuu().substring(10)))
                    v.addTransferFrom(t);
            }
        }
        newGraph.transfers.addAll(transfers);
        return new Pair<>(newGraph, keysMapping);
    }

    public Graph generateCleanedGraph(PopulatedPoint dep, PopulatedPoint dest) {
        Graph newGraph = clone();
        ArrayList<Transfer> transfers = newGraph.transfers;
        int previousVerticesCount;
        int previousTransfersCount;
        do {
            previousVerticesCount = newGraph.graph.size();
            previousTransfersCount = newGraph.transfers.size();
            Graph tGraph = newGraph.clone();
            for (Vertex v : newGraph.graph.values())
                if (v.getTransfersFrom().isEmpty() || v.getTransfersTo().isEmpty())
                    if ((!(v.getPopulatedPoint().getKoatuu().startsWith(dep.getKoatuu()))) && (!v.getPopulatedPoint().equals(dest)))
                        tGraph.removeVertex(v.getPopulatedPoint().getKoatuu());
            newGraph = tGraph;
            for (Transfer t : transfers) {
                if (!newGraph.graph.containsKey(t.getDestinationPointKoatuu()) && !t.getDestinationPointKoatuu().equals(dest.getKoatuu())) {
                    newGraph.removeTransfer(t);
                    continue;
                }
                boolean contained = false;
                for (Vertex v : newGraph.graph.values())
                    if (v.getTransfersFrom().contains(t))
                        contained = true;
                if (!contained)
                    newGraph.removeTransfer(t);
            }
        }
        while ((previousTransfersCount != newGraph.transfers.size()) || (previousVerticesCount != newGraph.graph.size()));
        if (newGraph.transfers.size() == 0)
            return new Graph();
        return newGraph;
    }

    public Vertex getVertexWithTransfers(String koatuu, DateTime dateTime) {
        ArrayList<Vertex> vertices = new ArrayList<>(Stream.of(graph.values()).filter(pp -> pp.getPopulatedPoint().getKoatuu().startsWith(koatuu)).toList());
        for (Vertex v : vertices) {
            if (v.getPopulatedPoint().getKoatuu().length() == 10) {
                vertices.remove(v);
                break;
            }
        }
        if (vertices.size() > 1)
            vertices = new ArrayList<>(Stream.of(vertices).sorted((o1, o2) -> Long.compare(Long.parseLong(o1.getPopulatedPoint().getKoatuu().substring(10)), Long.parseLong(o2.getPopulatedPoint().getKoatuu().substring(10)))).toList());
        long timeFrom = dateTime.getMillis();
        for (Vertex v : vertices) {
            if (timeFrom < Long.parseLong(v.getPopulatedPoint().getKoatuu().substring(10))) {
                return v;
            }
        }
        return null;
    }

    public List<Vertex> getVerticesToGetToTransfer(String koatuu, DateTime dateTime) {
        ArrayList<Vertex> found = new ArrayList<>();
        ArrayList<Vertex> vertices = new ArrayList<>(Stream.of(graph.values()).filter(pp -> pp.getPopulatedPoint().getKoatuu().startsWith(koatuu)).toList());
        for (Vertex v : vertices) {
            if (v.getPopulatedPoint().getKoatuu().length() == 10) {
                vertices.remove(v);
                break;
            }
        }
        if (vertices.size() > 1)
            vertices = new ArrayList<>(Stream.of(vertices).sorted((o1, o2) -> Long.compare(Long.parseLong(o1.getPopulatedPoint().getKoatuu().substring(10)), Long.parseLong(o2.getPopulatedPoint().getKoatuu().substring(10)))).toList());
        long timeFrom = dateTime.getMillis();
        for (Vertex v : vertices) {
            if (timeFrom >= Long.parseLong(v.getPopulatedPoint().getKoatuu().substring(10))) {
                found.add(v);
            }
        }
        return found;
    }

    public Graph clone() {
        Graph newGraph = new Graph();
        newGraph.graph.putAll(graph);
        ArrayList<Transfer> newTransfers = new ArrayList<>();
        for (Transfer t : transfers)
            newTransfers.add(t.clone());
        HashMap<String, PopulatedPoint> newPopulatedPoints = new HashMap<>();
        for (PopulatedPoint p : populatedPoints.values())
            newPopulatedPoints.put(p.getKoatuu(), p.clone());
        newGraph.transfers.addAll(newTransfers);
        newGraph.populatedPoints.putAll(newPopulatedPoints);
        return newGraph;
    }

}
