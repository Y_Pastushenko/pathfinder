package com.aeolus.pathfinder.model.network.impl;

import android.content.Context;
import android.os.AsyncTask;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Yuriy on 11.02.2018.
 */

public class GetPopulatedPointsTask extends AsyncTask<Context, Void, Void> {

    public interface OnPopulatedPointsReadyListener {
        void onPopulatedPointsReady(List<PopulatedPoint> populatedPoints);
    }

    private OnPopulatedPointsReadyListener listener;

    public Observable<ArrayList<PopulatedPoint>> getPopulatedPoints(Context context) {
        Observable<ArrayList<PopulatedPoint>> observable = Observable.create(emitter -> {
            listener = populatedPoints -> {
                emitter.onNext(new ArrayList<>(populatedPoints));
                emitter.onComplete();
            };
            this.execute(context);
        });
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    @Override
    protected Void doInBackground(Context... contexts) {
        Context context = contexts[0];
        try {
            URL url = new URL(context.getString(R.string.download_populated_places_txt_link));
            URLConnection ucon = url.openConnection();
            InputStream is = ucon.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
            byte[] buff = new byte[5 * 1024];
            int len;
            StringBuilder sb = new StringBuilder();
            while ((len = inStream.read(buff)) != -1) {
                sb.append(new String(buff, 0, len));
            }
            String json = sb.toString();
            Gson gson = new Gson();
            ArrayList<PopulatedPoint> populatedPoints = gson.fromJson(json, new TypeToken<ArrayList<PopulatedPoint>>() {
            }.getType());
            inStream.close();
            listener.onPopulatedPointsReady(populatedPoints);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            listener.onPopulatedPointsReady(null);
            return null;
        }
    }
}
