package com.aeolus.pathfinder.model.route;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;

import org.joda.time.DateTime;

import java.util.ArrayList;

/**
 * Created by Yuriy on 26.03.2018.
 */

public interface RouteManager {

    interface SearchEventsListener {

        void onAppendEdgeToRoutes(Transfer transfer);

        void onNewRouteFound(ArrayList<Transfer> route, double price);

        void onComplete();

        void onNoAvailablePathFound();
    }

    void search(PopulatedPoint departure, PopulatedPoint destination, DateTime from, RouteManagerImpl.SearchPriority priotity, SearchEventsListener listener);

    void stopSearch();

    ArrayList<ArrayList<Transfer>> getShownRoutes();
}
