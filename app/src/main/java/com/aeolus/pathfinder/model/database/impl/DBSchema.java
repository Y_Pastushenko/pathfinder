package com.aeolus.pathfinder.model.database.impl;

/**
 * Created by Yuriy on 11.02.2018.
 */

public class DBSchema {
    public static final String NAME = "PathFinderDB";

    public static final class PopulatedPointsTable {
        public static final String NAME = "populated_points";

        public static final class Columns {
            public static final String POPULATED_POINT_KOATUU = "koatuu";
            public static final String POPULATED_POINT_NAME = "name";
            public static final String POPULATED_POINT_LATITUDE = "latitude";
            public static final String POPULATED_POINT_LONGITUDE = "longitude";
            public static final String POPULATED_POINT_POPULATION = "population";
            public static final String POPULATED_POINT_POSTAL_CODE_START = "postal_codes_start";
            public static final String POPULATED_POINT_POSTAL_CODE_END = "postal_codes_end";
            public static final String POPULATED_POINT_WIKIPEDIA_URL = "wikipedia_url";
        }
    }
}
