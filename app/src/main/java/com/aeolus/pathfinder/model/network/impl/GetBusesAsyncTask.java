package com.aeolus.pathfinder.model.network.impl;

import android.content.Context;
import android.os.AsyncTask;

import com.aeolus.pathfinder.entities.Transfer;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Yuriy on 25.03.2018.
 */

public class GetBusesAsyncTask extends AsyncTask<Context, Void, Void> {

    private String departureKoatuu;
    private String destinationKoatuu;
    private LocalDate date;

    public interface OnTransfersReadyListener {
        void onTransfersReadyListener(List<Transfer> transfers);
    }

    private OnTransfersReadyListener listener;

    public Observable<ArrayList<Transfer>> getTransfers(Context context, String departureKoatuu, String destinationKoatuu, LocalDate date) {
        this.departureKoatuu = departureKoatuu;
        this.destinationKoatuu = destinationKoatuu;
        this.date = date;
        Observable<ArrayList<Transfer>> observable = Observable.create(emitter -> {
            listener = transfers -> {
                emitter.onNext(new ArrayList<>(transfers));
                emitter.onComplete();
            };
            this.execute(context);
        });
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    @Override
    protected Void doInBackground(Context... contexts) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.yy");
        ArrayList<Transfer> transfers = new ArrayList<>();
        try {
            Document doc = Jsoup.
                    connect("http://ticket.bus.com.ua/order/forming_bn?point_from=UA"
                            + departureKoatuu
                            + "&point_to=UA"
                            + destinationKoatuu
                            + "&date="
                            + formatter.print(date)
                            + "&date_add=0&fn=round_search")
                    .get();
            for (int j = 1; ; j++) {
                try {
                    Element element = doc.selectFirst("#trips > form > table:nth-child(1) > tbody > tr:nth-child(" + j + ") > td:nth-child(3)");
                    String departureTimeStr = element.text();
                    int i = departureTimeStr.indexOf(':');
                    LocalTime departureTime = LocalTime.parse(departureTimeStr.substring(i - 2, i + 3));

                    element = doc.selectFirst("#trips > form > table:nth-child(1) > tbody > tr:nth-child(" + j + ") > td:nth-child(8)");
                    String name = element.text();

                    element = doc.selectFirst("#trips > form > table:nth-child(1) > tbody > tr:nth-child(" + j + ") > td:nth-child(4)");
                    String arrivalTimeStr = element.text();
                    i = arrivalTimeStr.indexOf(':');
                    LocalTime arrivalTime = LocalTime.parse(arrivalTimeStr.substring(i - 2, i + 3));

                    DateTime departureDateTime = date.toDateTime(departureTime);

                    DateTime arrivalDateTime = arrivalTime.getMillisOfDay() > departureTime.getMillisOfDay() ? date.toDateTime(arrivalTime) : date.toDateTime(arrivalTime).plusDays(1);

                    element = doc.selectFirst("#trips > form > table:nth-child(1) > tbody >  tr:nth-child(" + j + ") > td:nth-child(5)");

                    Double price = Double.parseDouble(element.text());

                    transfers.add(new Transfer(name, Transfer.Type.BUS, departureKoatuu, destinationKoatuu, departureDateTime.getMillis(), arrivalDateTime.getMillis(), price));
                } catch (Exception e) {
                    break;
                }
            }
        } catch (Exception e) {
        }
        listener.onTransfersReadyListener(transfers);
        return null;
    }
}
