package com.aeolus.pathfinder.screens.main.interfaces;

import android.support.v4.util.Pair;

import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.general.root.RootView;

import java.util.ArrayList;

import io.reactivex.Observable;

/**
 * Created by mertsimsek on 25/05/2017.
 */

public interface MainView extends RootView {

    enum State {
        MAP_VIEW,
        SEARCH_FOR_ROUTE,
        SEARCH_FOR_POPULATED_POINT,
        POPULATION_POINT_CHOOSING,
        DETAILED_POPULATED_POINT,
        RESULTS_GOT,
        WAITING_FOR_RESULT,
        SEARCH_FINISHED,
        ABOUT_APP
    }

    void inflateDestinations(ArrayList<PopulatedPoint> destinations);

    void inflateDepartures(ArrayList<PopulatedPoint> departures);

    void drawRouteOnMap(ArrayList<PopulatedPoint> route);

    void setDepartureDate(ArrayList<Integer> dateTime);

//    Observable<PopulatedPoint> selectDeparture(ArrayList<PopulatedPoint> populatedPoints);
//
//    Observable<PopulatedPoint> selectDestination(ArrayList<PopulatedPoint> populatedPoints);

    Observable<PopulatedPoint> selectPopulatedPoint(ArrayList<PopulatedPoint> populatedPoints, String message);

    void appendRoute(ArrayList<Transfer> transfers, ArrayList<PopulatedPoint> populatedPoints, ArrayList<Pair<String, String>> pairs, double price, long totalTime);

    void showNoAvailableRoutsFoundMessage();

    void setState(State state);

    void showPopulatedPointDetailedDialog(PopulatedPoint populatedPoint);
}
