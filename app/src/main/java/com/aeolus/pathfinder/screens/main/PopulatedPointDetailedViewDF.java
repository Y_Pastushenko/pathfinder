package com.aeolus.pathfinder.screens.main;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.entities.PopulatedPoint;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PopulatedPointDetailedViewDF extends DialogFragment {

    public interface OnOpenWikiClickListener {
        void onOpenWikiClick(PopulatedPoint populatedPoint);
    }

    private OnOpenWikiClickListener onOpenWikiClickListener;

    private PopulatedPoint populatedPoint;

    @BindView(R.id.tv_pp_name)
    TextView ppNameTV;

    @BindView(R.id.tv_koatuu)
    TextView koatuuTV;

    @BindView(R.id.tv_latitude)
    TextView latitudeTV;

    @BindView(R.id.tv_longitude)
    TextView longitudeTV;

    @BindView(R.id.tv_population)
    TextView populationTV;

    @BindView(R.id.tv_postal_codes)
    TextView postalCodesTV;

    @OnClick(R.id.b_open_wikipedia)
    void onOpenWikiClick() {
        onOpenWikiClickListener.onOpenWikiClick(populatedPoint);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = View.inflate(getActivity(), R.layout.dialog_fragment_pp_detailed, null);
        ButterKnife.bind(this, v);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ppNameTV.setText(populatedPoint.getName());
        koatuuTV.setText(populatedPoint.getKoatuu());
        latitudeTV.setText(Double.toString(populatedPoint.getLatitude()));
        longitudeTV.setText(Double.toString(populatedPoint.getLongitude()));
        populationTV.setText(Integer.toString(populatedPoint.getPopulation()));
        String postalCodes = populatedPoint.getPostalCodesStart() + " - " + populatedPoint.getPostalCodesEnd();
        postalCodesTV.setText(postalCodes);
        return v;
    }

    public static PopulatedPointDetailedViewDF newInstance(OnOpenWikiClickListener onOpenWikiClickListener, PopulatedPoint populatedPoint) {
        PopulatedPointDetailedViewDF dialog = new PopulatedPointDetailedViewDF();
        dialog.onOpenWikiClickListener = onOpenWikiClickListener;
        dialog.populatedPoint = populatedPoint;
        return dialog;
    }
}
