package com.aeolus.pathfinder.screens.initialLoading.interfaces;

import com.aeolus.pathfinder.general.root.RootView;

/**
 * Created by mertsimsek on 25/05/2017.
 */

public interface InitialLoadingView extends RootView {

    void setDbPercent(int percent);
}
