package com.aeolus.pathfinder.screens.main;

import android.content.Context;

import com.aeolus.pathfinder.screens.main.interfaces.MainPresenter;
import com.aeolus.pathfinder.screens.main.interfaces.MainView;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by mertsimsek on 25/05/2017.
 */
@Module
public abstract class MainModule {

    @Provides
    static MainPresenter provideMainPresenter(Context context, MainView view) {
        return new MainPresenterImpl(context, view);
    }

    @Binds
    abstract MainView provideMainView(MainActivity activity);
}
