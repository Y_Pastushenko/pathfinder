package com.aeolus.pathfinder.screens.main.interfaces;

import com.aeolus.pathfinder.entities.PopulatedPoint;

import java.util.ArrayList;

/**
 * Created by mertsimsek on 25/05/2017.
 */

public interface MainPresenter {

    void onInit();

    void onCancelChoosingPPClick();

    void onStopSearchClick();

    void onCancelResultsClick();

    void onBeginningDepartureTyped(String beginning);

    void onBeginningDestinationTyped(String beginning);

    void onSearchPressed(String departure, String destination, ArrayList<Integer> dateTime);

    void onSearchForRouteClicked();

    void onPopulatedPointTyped(String name);

    void onSearchForPopulatePointClick();

    void onAboutClick();

    void onOpenWikiClick(PopulatedPoint populatedPoint);
}
