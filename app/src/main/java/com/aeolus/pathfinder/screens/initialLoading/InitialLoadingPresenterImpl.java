package com.aeolus.pathfinder.screens.initialLoading;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;

import com.aeolus.pathfinder.App;
import com.aeolus.pathfinder.model.database.DBManager;
import com.aeolus.pathfinder.model.network.NetworkManager;
import com.aeolus.pathfinder.model.preferences.PreferencesManager;
import com.aeolus.pathfinder.screens.initialLoading.interfaces.InitialLoadingPresenter;
import com.aeolus.pathfinder.screens.initialLoading.interfaces.InitialLoadingView;
import com.aeolus.pathfinder.screens.main.MainActivity;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


/**
 * Created by mertsimsek on 25/05/2017.
 */

public class InitialLoadingPresenterImpl implements InitialLoadingPresenter {

    private static final String DB_READY_KEY = "com.aeolus.pathfinder.screens.initialLoading.db_ready_key";

    private InitialLoadingView view;

    @Inject
    DBManager dbManager;

    @Inject
    NetworkManager networkManager;

    @Inject
    PreferencesManager preferenceManager;

    @Inject
    public InitialLoadingPresenterImpl(Context context, InitialLoadingView view) {
        this.view = view;
        ((App) context).getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onInit() {
        dbManager.init().observeOn(AndroidSchedulers.mainThread()).subscribe(b -> {
            if (preferenceManager.getBoolean(DB_READY_KEY))
                MainActivity.startActivity(view.getContext());
            else
                networkManager.getPopulatedPoints().observeOn(AndroidSchedulers.mainThread()).subscribe(pp ->
                        dbManager.inflatePopulatedPoints(pp).observeOn(AndroidSchedulers.mainThread()).subscribe(new Observer<Integer>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(Integer percent) {
                                view.setDbPercent(percent);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                                preferenceManager.saveBoolean(DB_READY_KEY, true);
                                MainActivity.startActivity(view.getContext());
                            }
                        }));
        });
    }

}
