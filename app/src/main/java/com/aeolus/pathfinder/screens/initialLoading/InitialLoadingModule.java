package com.aeolus.pathfinder.screens.initialLoading;

import android.content.Context;

import com.aeolus.pathfinder.screens.initialLoading.interfaces.InitialLoadingPresenter;
import com.aeolus.pathfinder.screens.initialLoading.interfaces.InitialLoadingView;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

/**
 * Created by mertsimsek on 25/05/2017.
 */
@Module
public abstract class InitialLoadingModule {

    @Provides
    static InitialLoadingPresenter provideMainPresenter(Context context, InitialLoadingView view) {
        return new InitialLoadingPresenterImpl(context, view);
    }

    @Binds
    abstract InitialLoadingView provideMainView(InitialLoadingActivity activity);
}
