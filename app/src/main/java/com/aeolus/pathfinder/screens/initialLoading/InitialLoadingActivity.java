package com.aeolus.pathfinder.screens.initialLoading;

import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.general.root.RootActivity;
import com.aeolus.pathfinder.screens.initialLoading.interfaces.InitialLoadingPresenter;
import com.aeolus.pathfinder.screens.initialLoading.interfaces.InitialLoadingView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InitialLoadingActivity extends RootActivity implements InitialLoadingView {

    @Inject
    InitialLoadingPresenter presenter;

    @BindView(R.id.tv_percents)
    TextView percentTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_loading);
        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onInit();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void setDbPercent(int percent) {
        percentTV.setText(String.format("%d%%",percent));
    }
}
