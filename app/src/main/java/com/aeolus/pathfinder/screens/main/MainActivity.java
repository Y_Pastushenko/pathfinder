package com.aeolus.pathfinder.screens.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.util.Pair;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.general.customViews.PButton;
import com.aeolus.pathfinder.general.customViews.PopulatedPointsAdapter;
import com.aeolus.pathfinder.general.customViews.RoutesListElement;
import com.aeolus.pathfinder.general.customViews.WrappedEditText;
import com.aeolus.pathfinder.general.root.RootActivity;
import com.aeolus.pathfinder.screens.main.interfaces.MainPresenter;
import com.aeolus.pathfinder.screens.main.interfaces.MainView;
import com.aeolus.pathfinder.screens.main.maps.MapsFragment;
import com.aeolus.pathfinder.screens.main.maps.MapsFragmentImpl;
import com.annimon.stream.Stream;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends RootActivity implements MainView, MapsFragment.OnMapReadyListener {

    @Inject
    MainPresenter presenter;

    private static final int ACTV_MIN_LENGTH = 4;

    MapsFragment mapsFragment;

    private WrappedEditText departure;

    private PopulatedPointsAdapter adapter;

    private State currentState;

    @BindView(R.id.actv_departure)
    AutoCompleteTextView departureACTV;

    @BindView(R.id.et_departure_time)
    EditText departureTimeET;

    @BindView(R.id.actv_destination)
    AutoCompleteTextView destinationACTV;

    @BindView(R.id.rl_root)
    RelativeLayout rootRL;

    @BindView(R.id.rl_search_block)
    RelativeLayout searchBlockRL;

    @BindView(R.id.iv_search)
    ImageView searchIV;

    @BindView(R.id.rl_select_populated_point)
    RelativeLayout selectPopulatedPointLL;

    @BindView(R.id.tv_select_populated_point_title)
    TextView selectPopulatedPointTV;

    @BindView(R.id.b_choose_pp_ok)
    PButton choosePPOkB;

    @BindView(R.id.rv_select_populated_point)
    RecyclerView recyclerViewSelectPP;

    @BindView(R.id.rl_optimal_route_details)
    RelativeLayout optimalRouteDetailsRL;

    @BindView(R.id.ll_steps_holder)
    LinearLayout routesHolderLL;

    @BindView(R.id.progress_bar)
    RelativeLayout progressBarRL;

    @BindView(R.id.rl_about_app)
    RelativeLayout aboutRL;

    @BindView(R.id.rl_search_for_populated_point)
    RelativeLayout searchForPopulatedPointRL;

    @BindView(R.id.et_s_f_pp)
    EditText searchForPPET;


//    @BindView(R.id.tv_total_time)
//    TextView totalTimeTV;
//
//    @BindView(R.id.tv_total_price)
//    TextView totalPriceTV;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.rl_cancel_general)
    RelativeLayout cancelGeneralRL;

    @OnClick(R.id.progress_bar)
    void onStopSearchClick() {
        presenter.onStopSearchClick();
    }

    @OnClick(R.id.iv_search)
    void onSearchClick() {
        if (departureACTV.getText().length() == 0 || destinationACTV.getText().length() == 0 || departure.getValues() == null) {
            showError("Empty fields");
            return;
        }
        presenter.onSearchPressed(departureACTV.getText().toString(), destinationACTV.getText().toString(), departure.getValues());
    }

    @OnClick(R.id.iv_swap)
    void onSwitchClick() {
        Editable dep = departureACTV.getText();
        departureACTV.setText(destinationACTV.getText());
        destinationACTV.setText(dep);
    }

    @OnClick(R.id.rl_cancel)
    void onCancelClick() {
        presenter.onCancelResultsClick();
    }

    @OnClick(R.id.rl_cancel_general)
    void onCancelGeneral() {
        clearEverything();
        setState(State.MAP_VIEW);
    }

    @OnClick(R.id.b_search_for_pp)
    void onSearchForPP() {
        if (searchForPPET.getText().length() != 0)
            presenter.onPopulatedPointTyped(searchForPPET.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        departureACTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == ACTV_MIN_LENGTH)
                    presenter.onBeginningDepartureTyped(s.toString());
            }
        });
        destinationACTV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == ACTV_MIN_LENGTH)
                    presenter.onBeginningDestinationTyped(s.toString());
            }
        });
        mapsFragment = MapsFragmentImpl.newInstance(this);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, mapsFragment.getFragment())
                .commit();
        departure = new WrappedEditText(departureTimeET);
        departure.setUpMask();
        navigationView.setOnDragListener((view, dragEvent) -> {
            setState(State.MAP_VIEW);
            return true;
        });
        navigationView.setNavigationItemSelectedListener(
                menuItem -> {
                    switch (menuItem.getItemId()) {
                        case R.id.nav_search_route:
                            presenter.onSearchForRouteClicked();
                            break;
                        case R.id.nav_search_populated_point:
                            presenter.onSearchForPopulatePointClick();
                            break;
                        case R.id.nav_about:
                            presenter.onAboutClick();
                            break;
                    }
                    drawerLayout.closeDrawers();
                    menuItem.setCheckable(false);
                    return true;
                });
        presenter.onInit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        departureACTV.setText("Львів");
        destinationACTV.setText("Житомир");
//        setDepartureDate(new ArrayList<>(Arrays.asList(2018, 6, 6, 0, 0)));
    }

    @Override
    public void onMapReady() {

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void close() {
        finish();
    }

    public static void startActivity(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    public void inflateDestinations(ArrayList<PopulatedPoint> destinations) {
        departureACTV.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, new ArrayList<>(Stream.of(destinations).map(PopulatedPoint::getKoatuu).toList())));
    }

    @Override
    public void inflateDepartures(ArrayList<PopulatedPoint> departures) {
        departureACTV.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, new ArrayList<>(Stream.of(departures).map(PopulatedPoint::getKoatuu).toList())));
    }

    @Override
    public void drawRouteOnMap(ArrayList<PopulatedPoint> route) {
        if (route.size() < 2)
            return;
        for (int i = 0; i < route.size() - 1; i++)
            mapsFragment.drawLine(route.get(i).getLatitude(), route.get(i).getLongitude(), route.get(i + 1).getLatitude(), route.get(i + 1).getLongitude(), 0, 0);
    }

    @Override
    public Observable<PopulatedPoint> selectPopulatedPoint(ArrayList<PopulatedPoint> populatedPoints, String message) {
        return showSelectPopulatedPoint(message, populatedPoints);
    }

    @Override
    public void setDepartureDate(ArrayList<Integer> dateTime) {
        departure.setValues(dateTime);
    }

    @Override
    public void showNoAvailableRoutsFoundMessage() {
        showError("No available routes found!");
    }

    @Override
    public void appendRoute(ArrayList<Transfer> transfers, ArrayList<PopulatedPoint> populatedPoints, ArrayList<Pair<String, String>> pairs, double price, long totalTime) {
        RoutesListElement routesListElement = new RoutesListElement(getContext()).setUp(transfers, populatedPoints, pairs, price, totalTime);
//        routesListElement.setOnClickListener(view -> {
//            mapsFragment.clear();
//            drawRouteOnMap(populatedPoints);
//        });
        ArrayList<View> childs = new ArrayList<>();
        for (int i = 0; i < routesHolderLL.getChildCount(); i++)
            childs.add(routesHolderLL.getChildAt(i));
        routesHolderLL.removeAllViews();
        routesHolderLL.addView(routesListElement);
        for (View view : childs)
            routesHolderLL.addView(view);
    }

    private Observable<PopulatedPoint> showSelectPopulatedPoint(String title, ArrayList<PopulatedPoint> populatedPoints) {
        Observable<PopulatedPoint> observable = Observable.create(emitter -> {
            selectPopulatedPointTV.setText(title);
            recyclerViewSelectPP.setLayoutManager(new LinearLayoutManager(getContext()));
            if (adapter == null)
                adapter = new PopulatedPointsAdapter(populatedPoint -> {
                    mapsFragment.flyTo(populatedPoint.getLatitude(), populatedPoint.getLongitude());
                    adapter.selectPopulatedPoint(populatedPoint);
                });
            adapter.setPopulatedPoints(populatedPoints);
            recyclerViewSelectPP.setAdapter(adapter);
            choosePPOkB.setOnClickListener(v -> {
                PopulatedPoint pp = adapter.getSelectedPopulatedPointAndClearSelection();
                if (pp == null)
                    Toast.makeText(MainActivity.this, "Nothing chosen", Toast.LENGTH_SHORT).show();
                else {
                    emitter.onNext(pp);
                    emitter.onComplete();
                }
            });
        });
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    @Override
    public void setState(State state) {
        if (state == null)
            return;
        if (state.equals(currentState))
            return;
        currentState = state;
        switch (state) {
            case MAP_VIEW:
                clearEverything();
                aboutRL.setVisibility(View.GONE);
                cancelGeneralRL.setVisibility(View.GONE);
                progressBarRL.setVisibility(View.GONE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.GONE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
            case RESULTS_GOT:
                aboutRL.setVisibility(View.GONE);
                cancelGeneralRL.setVisibility(View.GONE);
                progressBarRL.setVisibility(View.VISIBLE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.VISIBLE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
            case SEARCH_FOR_ROUTE:
                clearEverything();
                aboutRL.setVisibility(View.GONE);
                cancelGeneralRL.setVisibility(View.VISIBLE);
                progressBarRL.setVisibility(View.GONE);
                searchBlockRL.setVisibility(View.VISIBLE);
                optimalRouteDetailsRL.setVisibility(View.GONE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
            case POPULATION_POINT_CHOOSING:
                clearEverything();
                aboutRL.setVisibility(View.GONE);
                cancelGeneralRL.setVisibility(View.VISIBLE);
                progressBarRL.setVisibility(View.GONE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.GONE);
                selectPopulatedPointLL.setVisibility(View.VISIBLE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
            case WAITING_FOR_RESULT:
                clearEverything();
                aboutRL.setVisibility(View.GONE);
                cancelGeneralRL.setVisibility(View.GONE);
                progressBarRL.setVisibility(View.VISIBLE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.GONE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
            case SEARCH_FINISHED:
                aboutRL.setVisibility(View.GONE);
                cancelGeneralRL.setVisibility(View.GONE);
                progressBarRL.setVisibility(View.GONE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.VISIBLE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
            case ABOUT_APP:
                clearEverything();
                cancelGeneralRL.setVisibility(View.VISIBLE);
                aboutRL.setVisibility(View.VISIBLE);
                progressBarRL.setVisibility(View.GONE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.GONE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
            case SEARCH_FOR_POPULATED_POINT:
                clearEverything();
                cancelGeneralRL.setVisibility(View.VISIBLE);
                aboutRL.setVisibility(View.GONE);
                progressBarRL.setVisibility(View.GONE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.GONE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.VISIBLE);
                break;
            case DETAILED_POPULATED_POINT:
                clearEverything();
                cancelGeneralRL.setVisibility(View.GONE);
                aboutRL.setVisibility(View.GONE);
                progressBarRL.setVisibility(View.GONE);
                searchBlockRL.setVisibility(View.GONE);
                optimalRouteDetailsRL.setVisibility(View.GONE);
                selectPopulatedPointLL.setVisibility(View.GONE);
                searchForPopulatedPointRL.setVisibility(View.GONE);
                break;
        }
    }

    @Override
    public void showPopulatedPointDetailedDialog(PopulatedPoint populatedPoint) {
        PopulatedPointDetailedViewDF.newInstance(pp -> presenter.onOpenWikiClick(pp), populatedPoint).show(getSupportFragmentManager(), populatedPoint.getKoatuu());
    }

    private void clearEverything() {
        if (adapter != null)
            adapter.clear();
        departureACTV.setText("");
        destinationACTV.setText("");
        routesHolderLL.removeAllViews();
//        totalPriceTV.setText("");
//        totalTimeTV.setText("");
        mapsFragment.clear();
    }


}
