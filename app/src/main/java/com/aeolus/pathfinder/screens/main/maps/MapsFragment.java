package com.aeolus.pathfinder.screens.main.maps;


import android.support.v4.app.Fragment;

/**
 * Created by Yuriy on 11.02.2018.
 */

public interface MapsFragment {

    interface OnMapReadyListener {
        void onMapReady();
    }

    void drawLine(double lat1, double long1, double lat2, double long2, int color, int width);

    void flyTo(double latitude, double longitude);

    Fragment getFragment();

    void clear();
}
