package com.aeolus.pathfinder.screens.main;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.util.Pair;

import com.aeolus.pathfinder.App;
import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.model.database.DBManager;
import com.aeolus.pathfinder.model.network.NetworkManager;
import com.aeolus.pathfinder.model.route.Dijkstra;
import com.aeolus.pathfinder.model.route.Graph;
import com.aeolus.pathfinder.model.route.RouteManager;
import com.aeolus.pathfinder.model.route.RouteManagerImpl;
import com.aeolus.pathfinder.screens.main.interfaces.MainPresenter;
import com.aeolus.pathfinder.screens.main.interfaces.MainView;
import com.annimon.stream.Stream;

import org.joda.time.DateTime;
import org.joda.time.IllegalFieldValueException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.aeolus.pathfinder.screens.main.interfaces.MainView.State.DETAILED_POPULATED_POINT;
import static com.aeolus.pathfinder.screens.main.interfaces.MainView.State.POPULATION_POINT_CHOOSING;


/**
 * Created by mertsimsek on 25/05/2017.
 */

public class MainPresenterImpl implements MainPresenter {

    private MainView view;

    @Inject
    DBManager dbManager;

    @Inject
    NetworkManager networkManager;

    @Inject
    RouteManager routeManager;

    private volatile ArrayList<Transfer> cheapestTransfers;

    private volatile ArrayList<Pair<ArrayList<Transfer>, Integer>> variants;

    private AtomicInteger atomicInteger;

    @Inject
    public MainPresenterImpl(Context context, MainView view) {
        this.view = view;
        ((App) context).getAppComponent().inject(this);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onInit() {
        view.setState(MainView.State.MAP_VIEW);
//        dbManager
//                .getPopulatedPointsByKoatuus(new ArrayList<>(Arrays.asList("1810100000", "8000000000", "2310100000", "7410100000", "2110100000")))
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe((ArrayList<PopulatedPoint> capitals) -> {
//                    ArrayList<Transfer> transfers = new ArrayList<>();
//
//                    transfers.add(new Transfer("1", Transfer.Type.BUS, "1810100000", "8000000000", 90, 100, 5.0));
//                    transfers.add(new Transfer("2", Transfer.Type.BUS, "1810100000", "8000000000", 80, 90, 6.0));
//                    transfers.add(new Transfer("3", Transfer.Type.BUS, "1810100000", "2310100000", 80, 150, 6.0));
//                    transfers.add(new Transfer("4", Transfer.Type.BUS, "8000000000", "2310100000", 110, 160, 6.0));
//                    transfers.add(new Transfer("15", Transfer.Type.BUS, "8000000000", "2110100000", 120, 140, 6.0));
//                    transfers.add(new Transfer("5", Transfer.Type.BUS, "8000000000", "2310100000", 110, 120, 15.0));
//                    transfers.add(new Transfer("6", Transfer.Type.BUS, "8000000000", "2310100000", 110, 140, 15.0));
//                    transfers.add(new Transfer("10", Transfer.Type.BUS, "8000000000", "2310100000", 110, 110, 15.0));
//
//                    transfers.add(new Transfer("7", Transfer.Type.BUS, "2310100000", "7410100000", 150, 170, 15.0));
//                    transfers.add(new Transfer("8", Transfer.Type.BUS, "2310100000", "7410100000", 130, 160, 15.0));
//                    transfers.add(new Transfer("9", Transfer.Type.BUS, "2310100000", "7410100000", 145, 150, 15.0));
//
//                    PopulatedPoint dep = capitals.get(0);
//                    PopulatedPoint dest = capitals.get(3);
//
//                    Graph graph = new Graph();
//                    graph.inflate(capitals, transfers, () -> {
//                        android.util.Pair<Graph, HashMap<String, String>> resolvedGraphPair = graph.generateTimeResolvedGraph();
//                        Graph resolvedGraph = resolvedGraphPair.first;
//                        Graph cleaned = resolvedGraph.generateCleanedGraph(dep, dest);
//                        ArrayList<Transfer> lastStepTransfers = cleaned.getTransfersTo(dest.getKoatuu());
//                        ArrayList<Transfer> sortedLastStepTransfers = new ArrayList<>(Stream.of(lastStepTransfers).sorted((first, second) -> Long.compare(first.getArrivalDateTime(), second.getArrivalDateTime())).toList());
//
//                        cheapestTransfers = new ArrayList<>();
//                        variants = new ArrayList<>();
//                        atomicInteger = new AtomicInteger(0);
//
//
//                        for (int i = 0; i < sortedLastStepTransfers.size(); i++) {
//                            Transfer t = sortedLastStepTransfers.get(i);
//                            if (t != null) {
//                                getCheapestRouteToTransfer(t, i, cleaned, dest).subscribe(ts -> {
//                                    variants.add(ts);
//                                    if (variants.size() == sortedLastStepTransfers.size()) {
//                                        variants = new ArrayList<>(Stream.of(variants).sorted((first, second) -> Integer.compare(first.second, second.second)).toList());
//                                        ArrayList<ArrayList<Transfer>> result = new ArrayList<>(Stream.of(variants).map(p -> p.first).toList());
//                                    }
//                                });
//                            }
//                        }
//                    });
//                });
//        </editor-fold>
    }


    Observable<Pair<ArrayList<Transfer>, Integer>> getCheapestRouteToTransfer(Transfer t, int pos, Graph cleaned, PopulatedPoint dest) {
        Observable observable = Observable.create(emitter -> {
            List<Graph.Vertex> found = cleaned.getVerticesToGetToTransfer(t.getDeparturePointKoatuu(), new DateTime(t.getDepartureDateTime()));
            for (Graph.Vertex v : found) {
                PopulatedPoint resolvedDep = v.getPopulatedPoint();
                if (resolvedDep == null) {
                    continue;
                }
                new Dijkstra()
                        .getShortestPath(
                                resolvedDep,
                                dest,
                                new Dijkstra.DataProvider() {
                                    @Override
                                    public <V, E> V getDestinationVertex(E edge) {
                                        return (V) cleaned.getDestination((Transfer) edge);
                                    }

                                    @Override
                                    public <V, E> ArrayList<E> getEdges(V vertex) {
                                        return (ArrayList<E>) cleaned.getTransfersFrom((PopulatedPoint) vertex);
                                    }

                                    @Override
                                    public <E> double getPrice(E edge) {
                                        return ((Transfer) edge).getPrice();
                                    }

                                    @Override
                                    public <V> boolean vertexEquals(V vertex1, V vertex2) {
                                        return (vertex1).equals(vertex2);
                                    }
                                }, new Dijkstra.DijkstraEventsListener() {
                                    @Override
                                    public <E> void onAppendEdgeToRoutes(E edge) {

                                    }

                                    @Override
                                    public <E> void onOptimalRouteFound(ArrayList<E> route, double price) {
                                        ArrayList<Transfer> found = (ArrayList<Transfer>) route;
                                        atomicInteger.decrementAndGet();
                                        if (Stream.of(cheapestTransfers).mapToDouble(Transfer::getPrice).sum() > Stream.of((ArrayList<Transfer>) route).mapToDouble(Transfer::getPrice).sum()) {
                                            cheapestTransfers.clear();
                                            cheapestTransfers.addAll(found);
                                        }
                                        if (atomicInteger.intValue() == 0) {
                                            emitter.onNext(new Pair<>(cheapestTransfers, pos));
                                            emitter.onComplete();
                                        }
                                    }

                                    @Override
                                    public void onNoAvailablePathFound() {
                                        atomicInteger.decrementAndGet();
                                        if (atomicInteger.intValue() == 0) {
                                            emitter.onNext(new Pair<>(cheapestTransfers, pos));
                                            emitter.onComplete();
                                        }
                                    }
                                });
            }
        });
        observable.subscribeOn(Schedulers.io());
        return observable;
    }

    @Override
    public void onSearchForRouteClicked() {
        view.setState(MainView.State.SEARCH_FOR_ROUTE);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onSearchForPopulatePointClick() {
        view.setState(MainView.State.SEARCH_FOR_POPULATED_POINT);
//        dbManager
//                .getPopulatedPoint("8000000000")
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(pp -> view.showPopulatedPointDetailedDialog(pp));
    }

    @SuppressLint("CheckResult")
    @Override
    public void onPopulatedPointTyped(String name) {
        dbManager
                .getPopulatedPointsByName(name)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pp -> {
                            if (pp.size() == 0) {
                                view.showError("Нічого не знайдено!");
                                return;
                            }
                            view.setState(POPULATION_POINT_CHOOSING);
                            view.selectPopulatedPoint(new ArrayList<>(pp), "Оберіть населений пункт")
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(selectedPP -> {
                                        view.showPopulatedPointDetailedDialog(selectedPP);
                                        view.setState(DETAILED_POPULATED_POINT);
                                    });
                        }
                );
    }

    @Override
    public void onAboutClick() {
        view.setState(MainView.State.ABOUT_APP);
    }

    @Override
    public void onOpenWikiClick(PopulatedPoint populatedPoint) {
        view.getContext().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(populatedPoint.getWikipediaUrl())));
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBeginningDepartureTyped(String beginning) {
        dbManager
                .getPopulatedPointsBeginningWith(beginning)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(populatedPoints -> view.inflateDepartures(new ArrayList<>(populatedPoints)));
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBeginningDestinationTyped(String beginning) {
        dbManager
                .getPopulatedPointsBeginningWith(beginning)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(populatedPoints -> view.inflateDestinations(new ArrayList<>(populatedPoints)));
    }

    @SuppressLint("CheckResult")
    @Override
    public void onSearchPressed(String departure, String destination, ArrayList<Integer> dateTime) {
        DateTime date;
        try {
            date = new DateTime(dateTime.get(0), dateTime.get(1), dateTime.get(2), dateTime.get(3), dateTime.get(4), 0);
        } catch (IllegalFieldValueException e) {
            view.showError("Illegal date value");
            return;
        }
        dbManager.getPopulatedPointsBeginningWith(departure).observeOn(AndroidSchedulers.mainThread()).subscribe(dep -> {
            if (dep.size() == 0) {
                view.showError("No departure point found");
                return;
            } else {
                view.setState(POPULATION_POINT_CHOOSING);
                view
                        .selectPopulatedPoint(new ArrayList<>(dep), "Пункт відправлення")
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(departurePP ->
                                dbManager.getPopulatedPointsBeginningWith(destination).observeOn(AndroidSchedulers.mainThread()).subscribe(dest -> {
                                    if (dest.size() == 0) {
                                        view.showError("No destination point found");
                                        return;
                                    } else {
                                        view
                                                .selectPopulatedPoint(new ArrayList<>(dest), "Пункт призначення")
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(destinationPP -> {
                                                    view.setState(MainView.State.WAITING_FOR_RESULT);
                                                    routeManager.search(departurePP, destinationPP, date, RouteManagerImpl.SearchPriority.CHEAPEST, new RouteManager.SearchEventsListener() {
                                                        @Override
                                                        public void onAppendEdgeToRoutes(Transfer edge) {

                                                        }

                                                        @Override
                                                        public void onComplete() {
                                                            view.setState(MainView.State.SEARCH_FINISHED);
                                                        }

                                                        @Override
                                                        public void onNewRouteFound(ArrayList<Transfer> transfers, double price) {
                                                            ArrayList<String> koatuus = new ArrayList<>(Stream.of(transfers).map(Transfer::getDeparturePointKoatuu).toList());
                                                            koatuus.add(transfers.get(transfers.size() - 1).getDestinationPointKoatuu());
                                                            dbManager
                                                                    .getPopulatedPointsByKoatuus(koatuus)
                                                                    .observeOn(AndroidSchedulers.mainThread())
                                                                    .subscribe(populatedPoints -> updateResult(new ArrayList<>(transfers), populatedPoints, price));
                                                        }

                                                        @Override
                                                        public void onNoAvailablePathFound() {
                                                            view.showNoAvailableRoutsFoundMessage();
                                                            view.setState(MainView.State.MAP_VIEW);
                                                        }
                                                    });
                                                }, throwable -> {
                                                    return;
                                                });
                                    }
                                }), throwable -> {
                            return;
                        });
            }
        });
    }

    @Override
    public void onStopSearchClick() {
        view.setState(routeManager.getShownRoutes() == null || routeManager.getShownRoutes().isEmpty() ? MainView.State.MAP_VIEW : MainView.State.SEARCH_FINISHED);
        routeManager.stopSearch();
    }

    @Override
    public void onCancelChoosingPPClick() {
        view.setState(MainView.State.MAP_VIEW);
    }

    void setUpCurrentDepartureDate() {
        DateTime current = new DateTime();
        ArrayList<Integer> date = new ArrayList<>();
        date.add(current.getYear());
        date.add(current.getMonthOfYear());
        date.add(current.getDayOfMonth());
        date.add(current.getHourOfDay());
        date.add(current.getMinuteOfHour());
        view.setDepartureDate(date);
    }

    @SuppressLint("CheckResult")
    void updateResult(ArrayList<Transfer> transfers, ArrayList<PopulatedPoint> populatedPoints, double price) {
        dbManager
                .getPopulatedPointsByKoatuus(new ArrayList<>(
                        Stream
                                .of(transfers)
                                .map(Transfer::getDeparturePointKoatuu)
                                .toList()))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(departures ->
                        dbManager
                                .getPopulatedPointsByKoatuus(new ArrayList<>(
                                        Stream
                                                .of(transfers)
                                                .map(Transfer::getDestinationPointKoatuu)
                                                .toList()))
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(destinations -> {
                                    ArrayList<Pair<String, String>> names = new ArrayList<>();
                                    for (int i = 0; i < transfers.size(); i++)
                                        names.add(new Pair<>(departures.get(i).getName(), destinations.get(i).getName()));
                                    long totalTime = transfers.get(transfers.size() - 1).getArrivalDateTime() - transfers.get(0).getDepartureDateTime();
                                    view.setState(MainView.State.RESULTS_GOT);
                                    view.appendRoute(transfers, populatedPoints, names, price, totalTime);
                                }));
    }

    @Override
    public void onCancelResultsClick() {
        view.setState(MainView.State.MAP_VIEW);
        routeManager.stopSearch();
    }


}