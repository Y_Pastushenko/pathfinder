package com.aeolus.pathfinder.screens.main.maps;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.general.root.RootFragment;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MapsFragmentImpl extends RootFragment implements OnMapReadyCallback, MapsFragment {

    private GoogleMap map;

    private ArrayList<Polyline> polylines;

    private OnMapReadyListener onMapReadyListener;

    public static MapsFragment newInstance(OnMapReadyListener onMapReadyListener) {
        MapsFragmentImpl mapsFragment = new MapsFragmentImpl();
        mapsFragment.onMapReadyListener = onMapReadyListener;
        return mapsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        polylines = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        onMapReadyListener.onMapReady();
    }


    @Override
    public void drawLine(double lat1, double long1, double lat2, double long2, int color, int width) {
        polylines.add(map.addPolyline(new PolylineOptions()
                .add(new LatLng(lat1, long1), new LatLng(lat2, long2))
                .width(5)
                .color(Color.RED)));
    }

    @Override
    public void flyTo(double latitude, double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12);
        map.animateCamera(cameraUpdate);
    }

    @Override
    public Fragment getFragment() {
        return this;
    }

    @Override
    public void clear() {
        if (polylines == null)
            return;
        for (Polyline p : polylines)
            p.remove();
        polylines.clear();
    }
}


// Put this into manifest with api key instead of *apikey*
//
// <meta-data
// android:name="com.google.android.geo.API_KEY"
// android:value="*apikey*" />

