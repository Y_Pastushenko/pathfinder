package com.aeolus.pathfinder.screens.initialLoading.interfaces;

/**
 * Created by mertsimsek on 25/05/2017.
 */

public interface InitialLoadingPresenter {
    void onInit();
}
