package com.aeolus.pathfinder.general.customAnimations;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by y_pastushenko on 14.03.18.
 */

public class FadeInAnimation extends Animation {

    private long duration;

    private long delay;

    private View view;

    public FadeInAnimation(long duration, View view) {
        this.view = view;
        this.duration = duration;
        this.delay = 0;
    }

    public FadeInAnimation(long duration, long delay, View view) {
        this.view = view;
        this.duration = duration;
        this.delay = delay;
    }

    public FadeInAnimation(Context context, long duration, long delay, AttributeSet attrs, View view) {
        super(context, attrs);
        this.view = view;
        this.duration = duration;
        this.delay = delay;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        ValueAnimator colorAnimation = ValueAnimator.ofFloat(0f, 1f);
        colorAnimation.setDuration(duration);
        colorAnimation.setStartDelay(delay);
        view.setAlpha(0);
        view.setVisibility(View.VISIBLE);
        colorAnimation.addUpdateListener(animator -> view.setAlpha((Float) animator.getAnimatedValue()));
        colorAnimation.start();
    }
}
