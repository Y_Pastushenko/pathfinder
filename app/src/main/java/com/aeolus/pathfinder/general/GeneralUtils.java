package com.aeolus.pathfinder.general;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import org.joda.time.DateTime;

import java.util.ArrayList;

public final class GeneralUtils {

    public static int getPxFromDp(Context context, int sizeInDp) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = sizeInDp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }

    public static int getPxFromSp(Context context, int sizeInSp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sizeInSp, context.getResources().getDisplayMetrics());
    }

    public static double round(double d) {
        return ((double) Math.round(d * 100)) / 100;
    }

    public static float round(float d) {
        return ((float) Math.round(d * 100)) / 100;
    }

    public static void openInputToToET(EditText et, Context context) {
        et.requestFocus();
        ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void hideKeyboard(AppCompatActivity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static ArrayList<Fragment> getVisibleFragments(AppCompatActivity activity) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        ArrayList<Fragment> visibleFragments = new ArrayList<>();
        ArrayList<Fragment> fragments = new ArrayList<>(fragmentManager.getFragments());
        for (Fragment fragment : fragments) {
            if (fragment != null && fragment.isVisible())
                visibleFragments.add(fragment);
        }
        return fragments;
    }

    public static double distance(double lat1, double lat2, double lon1,
                                  double lon2, double el1, double el2) {
        final int R = 6371; // Radius of the earth
        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c * 1000; // convert to meters
        double height = el1 - el2;
        distance = Math.pow(distance, 2) + Math.pow(height, 2);
        return Math.sqrt(distance);
    }

    public static String getFormatedDateTime(long millis) {
        final String DIVIDER = "-";
        DateTime dateTime = new DateTime(millis);
        StringBuilder sb = new StringBuilder();
        sb.append(dateTime.getYear());
        sb.append(DIVIDER);
        sb.append(dateTime.getMonthOfYear());
        sb.append(DIVIDER);
        sb.append(dateTime.getDayOfMonth());
        sb.append(DIVIDER);
        sb.append(dateTime.getHourOfDay());
        sb.append(DIVIDER);
        sb.append(dateTime.getMinuteOfHour());
        return sb.toString();
    }

    public static float getHours(long millis) {
        return ((float) (Math.round((((double) millis) / 3600000) * 100))) / 100;
    }
}


