package com.aeolus.pathfinder.general.customViews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.aeolus.pathfinder.R;

import org.joda.time.LocalDate;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by y_pastushenko on 15.03.18.
 */

public class DateEditText extends FrameLayout {

    public interface CodeChangeListener {
        void onCodeTyped(String code);
    }

    private CodeChangeListener codeChangeListener;

    @BindView(R.id.year)
    EditText year;

    @BindView(R.id.month)
    EditText month;

    @BindView(R.id.day)
    EditText day;

    @BindView(R.id.hour)
    EditText hour;

    @BindView(R.id.minute)
    EditText minute;

    public DateEditText(@NonNull Context context) {
        super(context);
        init();
    }

    public DateEditText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DateEditText(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {
        inflate(getContext(), R.layout.view_code_et, this);
        ButterKnife.bind(this);
        year.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 4)
                    month.requestFocus();
            }
        });
        month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 2)
                    day.requestFocus();
            }
        });
        month.setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_DEL) {
                if (month.length() == 0)
                    year.requestFocus();
            }
            return false;
        });
        day.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 2)
                    hour.requestFocus();
            }
        });
        day.setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_DEL) {
                if (day.length() == 0)
                    month.requestFocus();
            }
            return false;
        });
        hour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 2) {
                    minute.requestFocus();
                }
            }
        });
        hour.setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_DEL) {
                if (hour.length() == 0)
                    minute.requestFocus();
            }
            return false;
        });
        minute.setOnKeyListener((view, i, keyEvent) -> {
            if (i == KeyEvent.KEYCODE_DEL) {
                if (hour.length() == 0)
                    day.requestFocus();
            }
            return false;
        });
    }

    public ArrayList<Integer> getValues() {
        if (year.getText().length() == 0 || month.getText().length() == 0 || day.getText().length() == 0 || hour.getText().length() == 0 || minute.getText().length() == 0)
            return null;
        ArrayList<Integer> values = new ArrayList<>();
        values.add(Integer.parseInt(year.getText().toString()));
        values.add(Integer.parseInt(month.getText().toString()));
        values.add(Integer.parseInt(day.getText().toString()));
        values.add(Integer.parseInt(hour.getText().toString()));
        values.add(Integer.parseInt(minute.getText().toString()));
        return values;
    }

    public void setCodeChangeListener(CodeChangeListener codeChangeListener) {
        this.codeChangeListener = codeChangeListener;
    }

    @SuppressLint("SetTextI18n")
    public void inflatetWithCurrentDate() {
        LocalDate localDate = new LocalDate();
        year.setText(Integer.toString(localDate.getYear()));
        month.setText(Integer.toString(localDate.getMonthOfYear()));
        day.setText(Integer.toString(localDate.getDayOfMonth()));
        hour.setText("0");
        minute.setText("0");
    }
}
