package com.aeolus.pathfinder.general.customAnimations;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by y_pastushenko on 14.03.18.
 */

public class FadeOutAnimation extends Animation {

    private long duration;

    private View view;

    public FadeOutAnimation(long duration, View view) {
        this.view = view;
        this.duration = duration;
    }

    public FadeOutAnimation(Context context, long duration, AttributeSet attrs, View view) {
        super(context, attrs);
        this.view = view;
        this.duration = duration;
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        ValueAnimator colorAnimation = ValueAnimator.ofFloat(1f, 0f);
        colorAnimation.setDuration(duration);
        colorAnimation.addUpdateListener(animator -> view.setAlpha((Float) animator.getAnimatedValue()));
        colorAnimation.start();
    }
}
