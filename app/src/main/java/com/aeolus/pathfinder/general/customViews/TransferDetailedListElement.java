package com.aeolus.pathfinder.general.customViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.general.GeneralUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransferDetailedListElement extends FrameLayout {

    @BindView(R.id.iv_vehicle_type)
    ImageView vehicleTypeIV;

    @BindView(R.id.tv_departure)
    TextView departureTV;

    @BindView(R.id.tv_destination)
    TextView destinationTV;

    @BindView(R.id.tv_departure_time)
    TextView departureTimeTV;

    @BindView(R.id.tv_arrival_time)
    TextView arrivalTimeTV;

    @BindView(R.id.tv_vehicle_type)
    TextView vehicleTypeTV;

    @BindView(R.id.tv_vehicle_name)
    TextView vehicleNameTV;

    @BindView(R.id.tv_price)
    TextView priceTV;

    @BindView(R.id.tv_position)
    TextView positionTV;

    public TransferDetailedListElement(@NonNull Context context) {
        super(context);
    }

    public TransferDetailedListElement(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TransferDetailedListElement(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TransferDetailedListElement setUp(Transfer transfer, String departureName, String destinationName, int position) {
        inflate(getContext(), R.layout.transfer_detailed_list_elemet, this);
        ButterKnife.bind(this);
        switch (transfer.getType()) {
            case BUS:
                vehicleTypeIV.setImageResource(R.drawable.ic_bus);
                break;
        }
        departureTV.setText(departureName);
        destinationTV.setText(destinationName);
        departureTimeTV.setText(GeneralUtils.getFormatedDateTime(transfer.getDepartureDateTime()));
        arrivalTimeTV.setText(GeneralUtils.getFormatedDateTime(transfer.getArrivalDateTime()));
        vehicleTypeTV.setText(transfer.getType().name().toLowerCase());
        vehicleNameTV.setText(transfer.getName());
        priceTV.setText(Double.toString(transfer.getPrice()));
        positionTV.setText(Integer.toString(position));
        return this;
    }
}
