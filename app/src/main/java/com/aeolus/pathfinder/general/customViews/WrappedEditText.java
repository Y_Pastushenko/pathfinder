package com.aeolus.pathfinder.general.customViews;

import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.widget.EditText;

import com.annimon.stream.Stream;

import java.util.ArrayList;

public class WrappedEditText {

    private static final String DIVIDER = "-";

    private static final int[] PATTERN = {4, 2, 2, 2, 2};

    private boolean[] added;

    boolean processTextChange = true;

    private EditText editText;

    private String textBeforeChange;

    public WrappedEditText(EditText editText) {
        this.editText = editText;
    }

    public void setUpMask() {
        added = new boolean[PATTERN.length - 1];
        int maxLength = 0;
        for (int i : PATTERN) {
            maxLength += i;
            maxLength += DIVIDER.length();
        }
        maxLength--;
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(maxLength);
        editText.setFilters(filters);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setOnClickListener(v -> editText.setSelection(editText.getText().length()));
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!processTextChange)
                    return;
                textBeforeChange = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!processTextChange)
                    return;
                boolean backspace = textBeforeChange.length() > editable.length();
                if (!backspace) {
                    if (editText.getSelectionEnd() != editable.length())
                        setTextProgrammatically(textBeforeChange);
                    String full = editText.getText().toString().replace(DIVIDER, "");
                    int position = full.length();
                    for (int i = 0; i < PATTERN.length - 1; i++) {
                        int pos = 0;
                        for (int j = 0; j < i + 1; j++)
                            pos += PATTERN[j];
                        if (position == pos) {
                            String toReplace = editText.getText().toString() + DIVIDER;
                            setTextProgrammatically(toReplace);
                        }
                    }
                } else {
                    if (editText.getSelectionEnd() != editable.length()) {
                        setTextProgrammatically(textBeforeChange);
                        return;
                    }
                    String str = editable.toString();
                    Character s = textBeforeChange.charAt(textBeforeChange.length() - 1);
                    if (!Character.isDigit(s))
                        while (!Character.isDigit(str.charAt(str.length() - 1)))
                            str = str.substring(0, str.length() - 1);
                    setTextProgrammatically(str);
                }
            }
        });
    }

    private void setTextProgrammatically(String text) {
        processTextChange = false;
        editText.setText(text);
        processTextChange = true;
        editText.setSelection(text.length());
    }

    public ArrayList<Integer> getValues() {
        if (editText.getText().length() == 0)
            return null;
        return new ArrayList<>(Stream.of(editText.getText().toString().split(DIVIDER)).map(Integer::parseInt).toList());
    }

    public boolean isEmpty() {
        return editText.getText().length() == 0;
    }

    public void setValues(ArrayList<Integer> dateTime) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < PATTERN.length; i++) {
            int v = dateTime.get(i);
            String sV = Integer.toString(v);
            if (sV.length() > PATTERN[i])
                return;
            if (sV.length() < PATTERN[i]) {
                StringBuilder sb = new StringBuilder(sV);
                while (sb.length() < PATTERN[i]) {
                    sb.insert(0, "0");
                }
                sV = sb.toString();
            }
            result.append(sV);
            result.append(DIVIDER);
        }
        result.deleteCharAt(result.length() - 1);
        setTextProgrammatically(result.toString());
    }
}
