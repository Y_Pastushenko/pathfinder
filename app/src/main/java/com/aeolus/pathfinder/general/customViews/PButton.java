package com.aeolus.pathfinder.general.customViews;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.aeolus.pathfinder.R;

public class PButton extends android.support.v7.widget.AppCompatButton {
    public PButton(Context context) {
        super(context);
        init();
    }

    public PButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    void init() {
        setBackground(ContextCompat.getDrawable(getContext(), R.drawable.button_back));
        setTextColor(getResources().getColor(R.color.button_text_color));
    }
}
