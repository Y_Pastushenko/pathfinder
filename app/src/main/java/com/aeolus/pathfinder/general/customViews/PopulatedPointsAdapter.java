package com.aeolus.pathfinder.general.customViews;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.entities.PopulatedPoint;

import java.util.ArrayList;

public class PopulatedPointsAdapter extends RecyclerView.Adapter<PopulatedPointsHolder> {

    public interface OnPopulatedPointsChosenListener {

        void onPopulatedPointsChosen(PopulatedPoint populatedPoint);

    }

    private OnPopulatedPointsChosenListener listener;

    private ArrayList<PopulatedPoint> populatedPoints = new ArrayList<>();

    private PopulatedPoint selectedPopulatedPoint;

    public PopulatedPointsAdapter(OnPopulatedPointsChosenListener listener) {
        this.listener = listener;
    }

    @Override
    public PopulatedPointsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_populated_points_list_item, parent, false);
        return new PopulatedPointsHolder(v);
    }

    @Override
    public void onBindViewHolder(PopulatedPointsHolder holder, int position) {
        holder.contentLL.setBackground(holder.itemView.getResources().getDrawable(R.drawable.cornered_back));
        if (selectedPopulatedPoint != null)
            if (selectedPopulatedPoint.equals(populatedPoints.get(position)))
                holder.contentLL.setBackground(holder.itemView.getResources().getDrawable(R.drawable.populated_point_list_selected));
        holder.bind(populatedPoints.get(position));
        holder.contentLL.setOnClickListener(view -> listener.onPopulatedPointsChosen(populatedPoints.get(position)));
    }

    @Override
    public int getItemCount() {
        if (populatedPoints == null)
            return 0;
        return populatedPoints.size();
    }

    public void setPopulatedPoints(ArrayList<PopulatedPoint> populatedPoints) {
        this.populatedPoints = populatedPoints;
        notifyDataSetChanged();
    }

    public void appendPopulatedPoints(ArrayList<PopulatedPoint> messages) {
        this.populatedPoints.addAll(messages);
        notifyDataSetChanged();
    }

    public void selectPopulatedPoint(PopulatedPoint populatedPoint) {
        if (!populatedPoints.contains(populatedPoint))
            return;
        selectedPopulatedPoint = populatedPoint;
        notifyDataSetChanged();
    }

    public PopulatedPoint getSelectedPopulatedPointAndClearSelection() {
        PopulatedPoint pp = selectedPopulatedPoint;
        selectedPopulatedPoint = null;
        return pp;
    }

    public void clear() {
        populatedPoints.clear();
        notifyDataSetChanged();
    }
}
