package com.aeolus.pathfinder.general.root;

import android.content.Context;

public interface RootView {

    Context getContext();

    void showError(String error);

    void close();
}
