package com.aeolus.pathfinder.general.customViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.entities.Transfer;
import com.aeolus.pathfinder.general.GeneralUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoutesListElement extends FrameLayout {

    @BindView(R.id.tv_time)
    TextView timeTV;
    @BindView(R.id.tv_price)
    TextView priceTV;
    @BindView(R.id.ll_transfers_holder)
    LinearLayout transfersHolderLL;
    @BindView(R.id.transfers_types_holder)
    LinearLayout transferTypesHolder;

    public RoutesListElement(@NonNull Context context) {
        super(context);
    }

    public RoutesListElement(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public RoutesListElement(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public RoutesListElement setUp(ArrayList<Transfer> transfers, ArrayList<PopulatedPoint> populatedPoints, ArrayList<Pair<String, String>> pairs, double price, long totalTime) {
        inflate(getContext(), R.layout.routes_list_elemet, this);
        ButterKnife.bind(this);
        timeTV.setText(String.format("%s годин в цілому", Float.toString(GeneralUtils.getHours(totalTime))));
        priceTV.setText(String.format("%s грн", Double.toString(price)));
        for (int i = 0; i < transfers.size(); i++) {
            transfersHolderLL.addView(new TransferDetailedListElement(getContext()).setUp(transfers.get(i), pairs.get(i).first, pairs.get(i).second, i + 1));
            switch (transfers.get(i).getType()) {
                case BUS:
                    ImageView imageView = new ImageView(getContext());
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(GeneralUtils.getPxFromDp(getContext(), 50), GeneralUtils.getPxFromDp(getContext(), 50));
                    imageView.setLayoutParams(layoutParams);
                    imageView.setImageResource(R.drawable.ic_bus);
                    transferTypesHolder.addView(imageView);
                    break;
            }
        }
        return this;
    }
}