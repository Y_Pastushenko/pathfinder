package com.aeolus.pathfinder.general.customViews;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aeolus.pathfinder.R;
import com.aeolus.pathfinder.entities.PopulatedPoint;
import com.aeolus.pathfinder.model.route.KOATUUResolver;

public class PopulatedPointsHolder extends RecyclerView.ViewHolder {

    LinearLayout contentLL;

    private TextView nameTV;

    private TextView oblastTV;

    private TextView raionTV;

    PopulatedPointsHolder(View view) {
        super(view);
        contentLL = view.findViewById(R.id.ll_content);
        nameTV = view.findViewById(R.id.tv_name);
        oblastTV = view.findViewById(R.id.tv_oblast_name);
        raionTV = view.findViewById(R.id.tv_raion_name);
    }

    public void bind(PopulatedPoint populatedPoint) {
        oblastTV.setText("");
        raionTV.setText("");
        nameTV.setText(populatedPoint.getName());
        if (KOATUUResolver.isOblastCenter(populatedPoint.getKoatuu()))
            return;
        oblastTV.setText(KOATUUResolver.getOblastName(populatedPoint.getKoatuu()));
        if (KOATUUResolver.isRaionCenter(populatedPoint.getKoatuu()))
            return;
        raionTV.setText(KOATUUResolver.getRaionName(populatedPoint.getKoatuu()));
    }

}
