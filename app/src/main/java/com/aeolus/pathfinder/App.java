package com.aeolus.pathfinder;

import com.aeolus.pathfinder.di.AppComponent;
import com.aeolus.pathfinder.di.DaggerAppComponent;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;


/**
 * Created by mertsimsek on 25/05/2017.
 */

public class App extends DaggerApplication {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent
                .builder()
                .application(this)
                .buildAppComponent();
        appComponent.inject(this);
        this.appComponent = appComponent;
        return appComponent;
    }

    public AppComponent getAppComponent(){
        return appComponent;
    }
}
