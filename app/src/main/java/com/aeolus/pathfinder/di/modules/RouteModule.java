package com.aeolus.pathfinder.di.modules;

import com.aeolus.pathfinder.model.database.DBManager;
import com.aeolus.pathfinder.model.network.NetworkManager;
import com.aeolus.pathfinder.model.route.Dijkstra;
import com.aeolus.pathfinder.model.route.KOATUUResolver;
import com.aeolus.pathfinder.model.route.RouteManager;
import com.aeolus.pathfinder.model.route.RouteManagerImpl;
import com.aeolus.pathfinder.model.route.SimplestTransfersFinder;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yuriy on 26.03.2018.
 */

@Module
public class RouteModule {

    @Provides
    RouteManager getRouteManager(SimplestTransfersFinder simplestTransfersFinder, DBManager dbManager) {
        return new RouteManagerImpl(simplestTransfersFinder, dbManager);
    }

    @Provides
    SimplestTransfersFinder getSimplestTransfersFinder(NetworkManager networkManager, DBManager dbManager) {
        return new SimplestTransfersFinder(networkManager, dbManager);
    }

    @Provides
    Dijkstra getDijkstra() {
        return new Dijkstra();
    }

    @Provides
    KOATUUResolver getKOATUUResolver() {
        return new KOATUUResolver();
    }

}
