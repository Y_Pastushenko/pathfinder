package com.aeolus.pathfinder.di;

import android.app.Application;

import com.aeolus.pathfinder.App;
import com.aeolus.pathfinder.di.modules.DBModule;
import com.aeolus.pathfinder.di.modules.NetworkModule;
import com.aeolus.pathfinder.di.modules.PreferencesModule;
import com.aeolus.pathfinder.di.modules.RouteModule;
import com.aeolus.pathfinder.screens.initialLoading.InitialLoadingPresenterImpl;
import com.aeolus.pathfinder.screens.main.MainPresenterImpl;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityBuilder.class,
        PreferencesModule.class,
        DBModule.class,
        NetworkModule.class,
        RouteModule.class})
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent buildAppComponent();

    }

    @Override
    void inject(DaggerApplication instance);

    void inject(App app);

    void inject(InitialLoadingPresenterImpl presenter);

    void inject(MainPresenterImpl presenter);

}
