package com.aeolus.pathfinder.di.modules;

import android.content.Context;

import com.aeolus.pathfinder.model.preferences.PreferencesManager;
import com.aeolus.pathfinder.model.preferences.PreferencesManagerImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {

    @Provides
    PreferencesManager getPreferencesManager(Context context) {
        return new PreferencesManagerImpl(context);
    }
}
