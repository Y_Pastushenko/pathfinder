package com.aeolus.pathfinder.di;

import com.aeolus.pathfinder.screens.initialLoading.InitialLoadingActivity;
import com.aeolus.pathfinder.screens.initialLoading.InitialLoadingModule;
import com.aeolus.pathfinder.screens.main.MainActivity;
import com.aeolus.pathfinder.screens.main.MainModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by mertsimsek on 25/05/2017.
 */
@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = InitialLoadingModule.class)
    abstract InitialLoadingActivity bindInitialLoadingActivity();

    @ContributesAndroidInjector(modules = {
            MainModule.class
    })
    abstract MainActivity bindMainActivity();
}
