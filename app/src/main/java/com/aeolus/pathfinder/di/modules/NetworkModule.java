package com.aeolus.pathfinder.di.modules;

import android.content.Context;

import com.aeolus.pathfinder.model.network.NetworkManager;
import com.aeolus.pathfinder.model.network.NetworkManagerImpl;
import com.aeolus.pathfinder.model.network.impl.GetBusesAsyncTask;
import com.aeolus.pathfinder.model.network.impl.GetPopulatedPointsTask;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yuriy on 26.03.2018.
 */

@Module
public class NetworkModule {

    @Provides
    NetworkManager getNetworkManager(Context context) {
        return new NetworkManagerImpl(context);
    }
}
