package com.aeolus.pathfinder.di.modules;

import android.content.Context;

import com.aeolus.pathfinder.model.database.DBManager;
import com.aeolus.pathfinder.model.database.DBManagerImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Yuriy on 26.03.2018.
 */

@Module
public class DBModule {

    @Provides
    @Singleton
    DBManager getDBManager(Context context) {
        return new DBManagerImpl(context);
    }

}
