package com.aeolus.pathfinder.entities;

import org.joda.time.DateTime;

import java.util.Objects;

public class Transfer {
    public enum Type {BUS}

    private String name;
    private Type type;
    private String departurePointKoatuu;
    private String destinationPointKoatuu;
    private long departureDateTime;
    private long arrivalDateTime;
    private double price;

    public Transfer() {
    }

    public Transfer(String name, Type type, String departurePointKoatuu, String destinationPointKoatuu, long departureDateTime, long arrivalDateTime, double price) {
        this.name = name;
        this.type = type;
        this.departurePointKoatuu = departurePointKoatuu;
        this.destinationPointKoatuu = destinationPointKoatuu;
        this.departureDateTime = departureDateTime;
        this.arrivalDateTime = arrivalDateTime;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDeparturePointKoatuu() {
        return departurePointKoatuu;
    }

    public void setDeparturePointKoatuu(String departurePointKoatuu) {
        this.departurePointKoatuu = departurePointKoatuu;
    }

    public String getDestinationPointKoatuu() {
        return destinationPointKoatuu;
    }

    public void setDestinationPointKoatuu(String destinationPointKoatuu) {
        this.destinationPointKoatuu = destinationPointKoatuu;
    }

    public long getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(long departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public long getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(long arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "name: " + name + "\n"
                + "type: " + type + "\n"
                + "Departure point: " + departurePointKoatuu + "\n"
                + "Destination point: " + destinationPointKoatuu + "\n"
                + "Departure datetime: " + new DateTime(departureDateTime).toString() + "\n"
                + "Destination datetime: " + new DateTime(arrivalDateTime).toString() + "\n"
                + "price: " + price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transfer transfer = (Transfer) o;
        return departureDateTime == transfer.departureDateTime &&
                arrivalDateTime == transfer.arrivalDateTime &&
                Double.compare(transfer.price, price) == 0 &&
                name.equals(transfer.name) &&
                type.equals(transfer.type) &&
                departurePointKoatuu.equals(transfer.departurePointKoatuu) &&
                destinationPointKoatuu.equals(transfer.destinationPointKoatuu);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, departurePointKoatuu, destinationPointKoatuu, departureDateTime, arrivalDateTime, price);
    }

    public Transfer clone() {
        return new Transfer(name, type, departurePointKoatuu, destinationPointKoatuu, departureDateTime, arrivalDateTime, price);
    }
}
