package com.aeolus.pathfinder.entities;

import java.util.List;

public class RouteElement {

    private List<Transfer> transfer;
    private String departureName;
    private String destinationName;
    private int position;

    public RouteElement(List<Transfer> transfer, String departureName, String destinationName, int position) {
        this.transfer = transfer;
        this.departureName = departureName;
        this.destinationName = destinationName;
        this.position = position;
    }

    public void setTransfer(List<Transfer> transfer) {
        this.transfer = transfer;
    }

    public void setDepartureName(String departureName) {
        this.departureName = departureName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
