package com.aeolus.pathfinder.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.aeolus.pathfinder.model.database.impl.DBSchema;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = DBSchema.PopulatedPointsTable.NAME)
public class PopulatedPoint {

    @SerializedName("koatuu")
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_KOATUU)
    private String koatuu;

    @SerializedName("name")
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_NAME)
    private String name;

    @SerializedName("latitude")
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_LATITUDE)
    private double latitude;

    @SerializedName("longitude")
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_LONGITUDE)
    private double longitude;

    @SerializedName("population")
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_POPULATION)
    private int population;

    @SerializedName("postalCodesStart")
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_POSTAL_CODE_START)
    private String postalCodesStart;

    @SerializedName("postalCodesEnd")
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_POSTAL_CODE_END)
    private String postalCodesEnd;

    @SerializedName("wikipediaUrl")
    @ColumnInfo(name = DBSchema.PopulatedPointsTable.Columns.POPULATED_POINT_WIKIPEDIA_URL)
    private String wikipediaUrl;

    public PopulatedPoint(String koatuu, String name, double latitude, double longitude, int population, String postalCodesStart, String postalCodesEnd, String wikipediaUrl) {
        this.koatuu = koatuu;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.population = population;
        this.postalCodesStart = postalCodesStart;
        this.postalCodesEnd = postalCodesEnd;
        this.wikipediaUrl = wikipediaUrl;
    }

    public String getKoatuu() {
        return koatuu;
    }

    public void setKoatuu(String koatuu) {
        this.koatuu = koatuu;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getPostalCodesStart() {
        return postalCodesStart;
    }

    public void setPostalCodesStart(String postalCodesStart) {
        this.postalCodesStart = postalCodesStart;
    }

    public String getPostalCodesEnd() {
        return postalCodesEnd;
    }

    public void setPostalCodesEnd(String postalCodesEnd) {
        this.postalCodesEnd = postalCodesEnd;
    }

    public String getWikipediaUrl() {
        return wikipediaUrl;
    }

    public void setWikipediaUrl(String wikipediaUrl) {
        this.wikipediaUrl = wikipediaUrl;
    }

    @Override
    public String toString() {
        return "koatuu: " + koatuu + "\n" +
                "name: " + name + "\n" +
                "longitude: " + longitude + "\n" +
                "latitude: " + latitude + "\n" +
                "population: " + population + "\n" +
                "postal codes: " + postalCodesStart + " - " + postalCodesEnd + "\n" +
                "wikipedia url: " + wikipediaUrl + "\n";
    }

    @Override
    public boolean equals(Object other) {
        return other != null && (other == this || other instanceof PopulatedPoint && this.koatuu.equals(((PopulatedPoint) other).getKoatuu()));
    }

    public PopulatedPoint clone() {
        return new PopulatedPoint(koatuu, name, latitude, longitude, population, postalCodesStart, postalCodesEnd, wikipediaUrl);
    }
}
